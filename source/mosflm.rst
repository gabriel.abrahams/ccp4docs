MOSFLM (CCP4: Supported Program)
================================

NAME
----

**mosflm** -MOSFLM version 7 for processing image plate and CCD data

SYNOPSIS
--------

**mosflm**

DESCRIPTION
-----------

`The Mosflm 7.0.3 User
Guide. <../x-windows/Mosflm/doc/mosflm_user_guide.html>`__

See also the `Mosflm Home
Page <http://www.mrc-lmb.cam.ac.uk/harry/mosflm>`__ for other help. In
particular, the `keyword
reference <http://www.mrc-lmb.cam.ac.uk/harry/cgi-bin/synopsis.cgi>`__
is very helpful.

AUTHORS
-------

| Andrew G.W. Leslie / Harry Powell
| MRC Laboratory of Molecular Biology
| Hills Road,
| Cambridge CB2 2QH
| UK

ACKNOWLEDGEMENTS
----------------

JPEG files can be written for diffraction images read by MOSFLM using
code which was originally written for a new Graphical User Interface.
This software is based in part on the work of the Independent JPEG
Group.
