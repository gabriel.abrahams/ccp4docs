|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • Hot Keys

.. raw:: html

   </div>

+-----------------+-----------+--------------------------------------------------+
| Windows/Linux   | Mac OSX   | Action                                           |
+=================+===========+==================================================+
| Ctrl-G          | ⌘-G       | PISA settings window                             |
+-----------------+-----------+--------------------------------------------------+
| Ctrl-O          | ⌘-O       | Open input file                                  |
+-----------------+-----------+--------------------------------------------------+
| Ctrl-L          | ⌘-L       | Download PDB entry and use it as an input file   |
+-----------------+-----------+--------------------------------------------------+

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
