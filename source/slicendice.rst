======================================
Slice-n-Dice (CCP4: Supported Program)
======================================


SYNOPSIS
--------

**Slice-n-Dice** is an automated MR pipeline designed to pre-process AlphaFold2 and RosettaFold models by removing low confidence regions and converting confidence scores into predicted B-factors. It also slices predicted models into distinct structural units and then automatically places the slices using Phaser. The slicing step can use AlphaFold2’s predicted aligned error (PAE) or can operate via a variety of Cɑ atom clustering algorithms, extending applicability to structures of any origin. 

Slice-n-Dice helps to deal with inaccuracy in domain-domain orientations of predicted models. 


Slice
-----

Clustering
++++++++++

Clustering algorithms are used to detect distinct structural units within a predicted model. 

Slice-n-Dice provides eight clustering methods for users to choose from:

**SciKitLearn**:

    cluster based on the coordinates of the Cɑ atoms:

    **Manual cluster selection**:

    #. `K-means <https://scikit-learn.org/stable/modules/clustering.html#k-means>`_
    #. `Birch clustering algorithm <https://scikit-learn.org/stable/modules/clustering.html#birch>`_
    #. `Agglomerative clustering <https://scikit-learn.org/stable/modules/clustering.html#hierarchical-clustering>`_
    #. `OPTICS <https://scikit-learn.org/stable/modules/clustering.html#optics>`_

    **Automatic cluster selection**:

    #. `DBSCAN <https://scikit-learn.org/stable/modules/clustering.html#dbscan>`_
    #. `Mean Shift <https://scikit-learn.org/stable/modules/clustering.html#mean-shift>`_

`Sklearn Clustering keywords`_

**CCTBX**: 

    cluster on the Predicted Aligned Error (PAE) produced by AlphaFold2 `(Baek et al., 2021)`_:

    #. PAE network
    #. PAE igraph

`CCTBX Clustering keywords`_

.. note:: CCTBX’s PAE methods automatically identify clusters, however, if a user defines a maximum number of splits, the closest clusters are merged until the number of splits ≤ maximum number of splits.  

 Based on preliminary data, the Birch algorithm is the current default but further benchmarking will be done.


----
Dice
----

Dice method performs molecular replacement on the individual slices produced by Slice by :doc:`Phaser <phaser>`. Phaser automatically places as many slices as possible.  

:doc:`Molrep <molrep>` `Phased Translation Function (PTF) mode <http://journals.iucr.org/d/issues/2010/01/00/dz5175/>`_ is used to try and place the slices that could not be positioned in the initial Phaser job. After each MOLREP job, :doc:`REFMAC5 <refmac5>` is used to assess if the placed slice has improved the solution. 

`Molecular Replacement specific options keywords`_

KEYWORDED INPUT
---------------

Optional arguments
++++++++++++++++++

    ``-h, --help``

            show this help message and exit

Basic options
+++++++++++++

    ``-xyzin`` **XYZIN**

            Path to XYZ file **(default: None)**

    ``-xyz_list`` **XYZ_LIST [XYZ_LIST ...]**

            List of XYZ files that represent unique regions in a target structure **(default: None)**

    ``-hklin`` **HKLIN**

            Path to HKL file **(default: None)**

    ``-mapin`` **MAPIN**
  
            Path to MAP/MRC file **(default: None)**

    ``-seqin`` **SEQIN**
  
            Path to sequence file **(default: None)**

    ``-xyz_source {pdb,alphafold,alphafold_bfactor,rosetta}``

            Source of the XYZ file **(default: pdb)**

    ``-plddt_threshold`` **PLDDT_THRESHOLD**

            Removes residues from Alphafold models below this pLDDT threshold **(default: 70)**

    ``-rms_threshold`` **RMS_THRESHOLD**

            Removes residues from Rosetta models below this RMS threshold **(default: 1.75)**


.. _Sklearn Clustering keywords:

Sklearn Clustering options
++++++++++++++++++++++++++

    ``-clustering_method {agglomerativeclustering,birch,dbscan,kmeans,meanshift,optics,pae_networkx,pae_igraph}``

            Select which clustering method to use **(default: birch)**

    ``-bandwidth`` **BANDWIDTH**
            
            Set the bandwidth value used by MeanShift in the RBF kernel **(default: None)**
            
    ``-eps_value`` **EPS_VALUE**
    
            Set the epsilon value used by DBSCAN that defines how
            close together points need to be to be considered part of a cluster **(default: 10)**

    ``-min_splits`` **MIN_SPLITS**

            Set the minimum number of splits **(default: 1)**

    ``-max_splits`` **MAX_SPLITS**

            Set the maximum number of splits **(default: 3)**

    ``-xyz_list_splits`` **XYZ_LIST_SPLITS [XYZ_LIST_SPLITS ...]**

            Provide a list of splits relating to each entry in xyz_list. **(default: None)**

.. _CCTBX Clustering keywords:

PAE Clustering options
++++++++++++++++++++++

    ``-graph_resolution`` **GRAPH_RESOLUTION**
        
            Regulates how aggressively the clustering algorithm is. Smaller values lead to larger clusters. Value should be larger than zero, and values larger than 5 are unlikely to be useful. **This is only used by pae_networkx or pae_igraph clustering methods** **(default: 1)**

    ``-pae_cutoff`` **PAE_CUTOFF**

            Graph edges will only be created for residue pairs with pae<pae_cutoff. This is only used by pae_networkx or pae_igraph clustering methods **(default: 5)**

    ``-pae_json_file`` **PAE_JSON_FILE**

            Path to PAE json file. This is required if using pae_networkx or pae_igraph clustering methods **(default: None)**

    ``-pae_power`` **PAE_POWER**
            Each edge in the graph will be weighted proportional to (1/pae**pae_power) This is only used by pae_networkx or pae_igraph clustering methods **(default: 1)**

Job submission options
++++++++++++++++++++++

    ``-nproc`` **NPROC**

            Number of processors. For local, serial runs the jobs will be split across nproc processors. For cluster submission, this should be the number of processors on a node. **(default: None)**

    ``-submit_qtype {local,lsf,pbs,slurm,sge,torque}``

            The job submission queue type (default: local)

    ``-submit_queue`` **SUBMIT_QUEUE**

            The queue to submit to on the cluster. **(default: None)**

.. _Molecular Replacement specific options keywords:

Molecular Replacement specific options
++++++++++++++++++++++++++++++++++++++

    ``-sga {all,enant,none}``, ``--sgalternative {all,enant,none}``
    
            Check alternative space groups (default: none)

    ``-ncyc`` **NCYC**
    
            The number of refinement cycles to run (default: 10)

    ``-no_mols`` **NO_MOLS**
    
          Set the number of molecules (default: 1)

    ``-molrep_tf {PTF,SAPTF}``

            Type of phased translation function to use in Molrep (only used in intensive mode) (default: PTF)

    ``-pack_cutoff`` **PACK_CUTOFF**

            Limit on total number (or percent) of clashes **(default: 50)**

    ``-peak_rot_cutoff`` **PEAK_ROT_CUTOFF**

            Cutoff value for the rotation function peak selection criteria. **(default: 75)**

    ``--intensive_mode``

            Perform an intensive MR mode, iteratively attempts to place search models with phaser and molrep until the best solution is found **(default: False)**

    ``-llg_cutoff`` **LLG_CUTOFF**

            Set LLG cutoff score used when determining if a solution is correctly placed **(default: 45)**

    ``-tfz_cutoff`` **TFZ_CUTOFF**

            Set TFZ cutoff score used when determining if a solution is correctly placed **(default: 7)**



**ACKNOWLEDGEMENTS**

This article uses materials kindly provided by Dr. Adam Simpkin and Dr. Ronan Keegan, whose help is greatly appreciated.

**References**

`Vagin, A. & Teplyakov, A. (2010). Molecular replacement with MOLREP, pp. 22-25, Acta Cryst. D66 <http://journals.iucr.org/d/issues/2010/01/00/dz5175/>`_

`Murshudov, G.N., Skubak, P., Lebedev, A.A., Pannu, N.S., Steiner, R.A., Nicholls, R.A., Winn, M.D., Long, F., and Vagin, A.A. (2011)  Acta Cryst. D67: 355-367; <https://doi.org/10.1107/S0907444911001314>`_

`McCoy, A.J., Grosse-Kunstleve, R.W., Adams, P.D., Winn, M.D., Storoni, L.C., Read R.J. (2007) Phaser Crystallographic Software. J. Appl. Cryst. 40: 658-674; <https://doi.org/10.1107/S0021889807021206>`_

.. _(Baek et al., 2021):

`Baek, M., DiMaio, F., Anishchenko, I., Dauparas, J., Ovchinnikov, S., Lee, G. R., Wang, J., Cong, Q., Kinch, L. N., Schaeffer, R. D., Millán, C., Park, H., Adams, C., Glassman, C. R., DeGiovanni, A., Pereira, J. H., Rodrigues, A. V., van Dijk, A. A., Ebrecht, A. C., Opperman, D. J., Sagmeister, T., Buhlheller, C., Pavkov-Keller, T., Rathinaswamy, M. K., Dalwadi, U., Yip, C. K., Burke, J. E., Garcia, K. C., Grishin, N. V., Adams, P. D., Read, R. J. & Baker, D. (2021). Science. 373, 871–876. <https://doi.org/10.1126%2Fscience.abj8754>`_

