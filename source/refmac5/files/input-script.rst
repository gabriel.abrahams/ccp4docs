REFMAC (CCP4: Supported Program)
================================

User's manual for the program REFMAC, version 5.\*
--------------------------------------------------

Input and output files - Input script
-------------------------------------

A REFMAC input script has the same form as the most of the
`**CCP4** <../../INDEX.html>`__ programs; *i.e.*:

    ::

        refmac logical_file1 <actual_file1> logical_file2 <actual_file2> \
        ... << eor

        Command lines

        END
        eor

Where ``logical_filei`` are logical names of files to be used and
``actual_filei`` are actual file names.

| For example:
| Restrained refinement with ``file_crd.pdb`` as input coordinate file
  and ``file_mtz.mtz`` as input reflection file and results written to
  output files ``file_refmac.mtz`` (reflection file to be used for map
  caclulation) and ``file_refmac.pdb`` (coordinate file):

    ::

        refmac \
        hklin file_mtz.mtz \
        xyzin file_crd.pdb  \
        hklout file_refmac.mtz \
        xyzout file_refmac.pdb << eor

        Command lines
         
        END
        eor

Logical names and description of possible input and output files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

HKLIN
    Input reflection file. Format MTZ.
HKLOUT
    Output reflection file. Format MTZ. Contains coefficients for map
    calculations.
XYZIN
    Input coordinate file. Preferred format PDB. mmCIF can also be used.
    From mmCIF, REFMAC uses only information necessary for refinement.
XYZOUT
    Output coordinate file. Current format is PDB.
`TLSIN <tls.html#tlsin>`__
    Input file containing TLS parameters.
`TLSOUT <tls.html#tlsout>`__
    Output file containing new refined TLS parameters.
SYMOP
    File containing symmetry operations. Only set explicitly if
    non-default symmetry libraries are used.
LIBIN
    Input user supplied library of ligand descriptions.
LIBOUT
    Output library of descriptions. It will be written if new ligand is
    encountered.
ATOMSF
    File containing atomic form factors. Only set explicitly if
    non-default atomic structure factor libraries are used.

Command lines
~~~~~~~~~~~~~

Command lines could directly contain keywords as described in the
`**Keyworded input** <../keywords/index.html>`__ section.

Command lines could also contain a file of command lines. In this case a
@ sign should preceed the file name. For example:

    ::

        refmac hklin file_mtz.mtz  \
               xyzin file_crd.pdb  \
               hklout file_refmac.mtz \
               xyzout file_refmac.pdb << eor
        LABIn ....
           
        @my_favourite_defaults_for_low_resolution
         
        ....
        END
        eor

Then "``my_favourite_defaults_for_low_resolution``" could contain
following the keywords:

    ::

        #
        #   Do not use hydrogens even if they are in the input file
        #
        MAKE_restraints HYDRogens No
        #
        #   Set all B values to 30 before starting refinement
        #
        BFACtors SET_to 30
        #
        #Do 10 cycle of TLS refinement before individual atomic
        #
        REFInement TLSCycle 10 
        #
        # Do overall B value refinement
        #
        BREFinement OVERall 
        #
        # Use tight restraints
        #
        WEIGt MATRix 0.1 
        #
        # Fix Babinet's B value to 200
        #
        SCALe LSSCale FIXBulk BBULk 200
        #
        #   Damp down shifts by 0.3 at every cycle
        #
        DAMPing_factors 0.3 0.3

See `**Examples** <../usage/examples.html>`__ section for scripts for
various purposes.
