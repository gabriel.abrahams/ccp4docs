CCP4 v8.0 Program Changes
=========================

**Alphafold models for Molecular Replacement**

CCP4 8.0 provides a seamless integration of Alphafold models for Molecular Replacement, which considerably increases chances of Molecular Replacement. All components that **prepare MR models from PDB entries**, such as **MrBump** or **MrParse**, will now do the same **using the Alphafold database at the EBI** (internet connection required). 

This is reflected in **CCP4i2** and **CCP4 Cloud** interfaces, which will also **re-calculate the B-factor columns in Alphafold files** for proper weighting in MR software (Phaser and Molrep). 



**New programs:**

- Modelcraft 2.2.3

- IRIS 

- PAIREF

- PDB-REDO Tools


**Program updates:**

-  COOT 0.9.8.1

-  DIALS 2.8 - includes scaling module

-  DUI 2021.11.1

-  XIA2 3.8.0

-  PHASER 2.8.3

-  SHELX 

-  Buccaneer

-  CCP4i2

-  CCP4 Cloud
