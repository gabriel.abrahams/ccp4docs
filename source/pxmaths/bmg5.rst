|CCP4 web logo|

Basic Maths for Protein Crystallographers

Resolution

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

| The length of the reciprocal lattice vector
  h\ **a\ :sup:`\*`**\ +k\ **b\ :sup:`\*`**\ +l\ **c\ :sup:`\*`** is
  called d\ :sup:`\*` and equals |2sinthetaoverlambda|.
| Crystallographers often define \|\ **S**\ \|² as (d:sup:`\*`)² or
  |4sinsqthetaoverlambdasq|. **S**, a vector, is defined as
  **s**-**s**\ :sub:`0`, where **s** is in the direction of the
  scattered wave, and **s**\ :sub:`0` is in the direction of the
  incident wave.
| Protein crystallographers talk about resolution in terms of
  1/d\ :sup:`\*` (or d), hence the 'high resolution' limit is actually a
  small number.

| 

--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg6.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg4.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |2sinthetaoverlambda| image:: ../images/2sinthetaoverlambdatr.gif
   :width: 59px
   :height: 16px
.. |4sinsqthetaoverlambdasq| image:: ../images/4sinsqthetaoverlambdasqtr.gif
   :width: 71px
   :height: 16px
