CCP4 v6.5 Program Changes
=========================

**New programs:**

-  `BLEND <blend.html>`__: multi-dataset combination and cluster
   analysis
-  CRANK-2: redesigned Crank pipeline for experimental phasing
-  FECKLESS: combination of multilattice files from Mosflm
-  `PRIVATEER-VALIDATE <privateer.html>`__: validation of carbohydrate
   structures in ED maps
-  ACEDRG: monomer description generator based on COD
-  LIBG: generator of external restraints for DNA/RNA
-  `PRELYSCAR <prelyscar_ccp4.html>`__: prediction of lysine
   carboxylation

**Program updates:**

-  AIMLESS 0.3.11 - options for XFEL data, improved SDcorrection, and
   more
-  POINTLESS 1.9.23 - bug fixes
-  REFMAC 5.8.0103 - multiple ehnancements and bug fixes
-  PROSMART 0.845 - handles also DNA/RNA chains now, and other
   ehnancements
-  BUCCANEER 1.6.0 - new modes for building after MR
-  CTRUNCATE 1.16.5 - multiple ehnancements and bug fixes
-  COOT 0.8.1 - multiple ehnancements and bug fixes
-  CCP4MG 2.10.0 - included in the CCP4 suite (previously a separate
   package)
-  MrBUMP - sequence-based searching is using now HMMER by default
-  AMPLE - supports QUARK generated decoys for use in MR
