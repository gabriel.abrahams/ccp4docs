MAPMASK (CCP4: Supported Program)
=================================

NAME
----

**mapmask** - map/mask extend program

SYNOPSIS
--------

| **mapmask MAPIN/MSKIN/MAPIN1/MSKIN1** *foo.map* [ **MAPIN2/MSKIN2**
  *bar.map* ] [ **XYZIN** *foo.pdb* ] [ **MAPOUT** *foobar.map* ] [
  **MSKOUT** *foobar.msk* ]
| [`Keyworded Input <#keywords>`__]

 DESCRIPTION
------------

\`mapmask' is a general map and mask manipulation program. It will
change the extent of a map or mask file, re-order a map or mask, scale a
map or mask, and generate a mask from a map and vice versa. It will also
produce a print out of arbitrary map or mask sections. It can combine
two input maps or masks to produce a sum or product map, or a solvent
flattened/flipped map.

It can be used in conjunction with `SFALL <sfall.html>`__ to make a
solvent mask, which can be used for solvent flattening using
`'``dm``' <dm.html>`__ or FLATMAP. In this case SFALL is used to make a
atom map, and then MAPMASK is used to mask the non-zero areas of this
map.

Scaling options from Andrew Leslie's FLATMAP program can be used to
scale the map for solvent flattening (SCALE card). Jan Pieter Abraham's
solvent flipping and density truncation techniques may be applied (SOLV
card).

 INPUT/OUTPUT FILES
-------------------

 MAPIN/MAPIN1/MSKIN/MSKIN1
~~~~~~~~~~~~~~~~~~~~~~~~~~

Input map or mask. If MAPIN or MAPIN1 is specified, the values of the
map grid points will converted to real numbers (even if the input file
is a mask). If MSKIN or MSKIN1 is specified, the values of the grid
points will be set to 0 or 1 (even if the input file is a real map).

 MAPIN2/MSKIN2
~~~~~~~~~~~~~~

Second input map, used when combining maps or performing solvent
flattening (used by `SOLV <#solv>`__, `MAPS <#maps>`__ cards). Must be
on the same grid as the first input map.

MAPLIM
~~~~~~

is for use with `XYZLIM MATCH <#xyzlim>`__ option.

XYZIN
~~~~~

is for use with `BORDER <#border>`__ option.

MAPOUT
~~~~~~

is an output map.

MSKOUT
~~~~~~

is an output mask, 1 wherever MAP>MSKCUT and 0 elsewhere.

Either MAPOUT, or MSKOUT, or neither, or both may be assigned. If both
are assigned then MAPOUT will contain the real map values and MSKOUT the
values 0/1.

.. _keywords:

KEYWORDED INPUT
---------------

Possible keywords are:

    `AXIS <#axis>`__, `BORDER <#border>`__,
    `EXTEND <#extend>`__, `MAPS <#maps>`__,
    `MASK <#mask>`__, `MODY <#mode>`__, `PAD <#pad>`__,
    `PRINT <#print>`__, `SCALE <#scale>`__,
    `SOLV <#solv>`__, `SYMMETRY <#symmetry>`__,
    `XYZLIM <#xyzlim>`__.

XYZLIM [ASU] [CELL] [MATCH] <x1> <x2> <y1> <y2> <z1> <z2>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set the output map extent as \`extend'. <x1>-<z2> are given in grid
units or in fractional coordinates. It is possible to automatically
extend to the CCP4 default asymmetric unit, or a whole unit cell, by
specifying \`XYZLIM ASU' or \`XYZLIM CELL'. It is also possible to
extend the map to match another map (given as MAPLIM) by specifiying
\`XYZLIM MATCH'. The default is to keep the extent of the input map.

.. _border:

BORDER <n>
~~~~~~~~~~

Extend the map to cover the volume of a coordinate model given in XYZIN,
as \`extend'. <n> is the border in Angstrom around the edge of the
model.

.. _extend:

EXTEND COPY \| XTAL \| OVERLAP
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Selects how the map is to be extended.

 COPY
    copies density from the input to the output map without applying
    symmetry or cell repeat.
 XTAL
    applies both symmetry and cell repeat to generate density outside
    the input map extent
 OVERLAP
    applies both symmetry and cell repeat to generate density, and when
    multiple values are available for a grid point, the highest is
    taken. This option can be used to generate a solvent mask from an
    averaging mask covering the whole complex.

Default: If the input map contains the output map: EXTEND COPY. If the
input map must be extended: EXTEND XTAL.

.. _pad:

PAD <rho>
~~~~~~~~~

Normally if you try to extend a map which does not contain at least an
(arbitrary) asymmetric unit, mapmask will give an error since it will be
impossible to generate some of the map. PAD <rho> suppresses the error
message and specifies a density value <rho> to be used for such regions.

.. _symmetry:

SYMMETRY <spacegroup name or number>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Override the spacegroup of the input map.

.. _axis:

AXIS <fast> <medium> <slow>
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Change the output axis order. Default is input axis order. <fast>,
<medium>, <slow> must be some permutation of X Y Z.

.. _mask:

MASK [CUT <mskcut>] [FRAC <mskfrc>] [VOLUME <mskvol>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set the threshold value for making a mask from a map: all density values
above this level in the final map will be set to 1.0 in the output mask
if one is specified.

 CUT <mskcut>
    sets the density cutoff value directly.
 FRAC <mskfrc>
    sets the fraction of the output mask that should be above the
    threshold. This is used when making a solvent mask, and will work
    correctly on a unit cell or asymmetric unit.
 VOLUME <mskvol>
    sets the volume of the mask that should be above the threshold as a
    fraction of the unit cell volume. This is used when making an
    averaging mask from a correlation map (see
    `MAPROT <maprot.html>`__).

MODE <logical\_name1> [ <logical\_name2> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| [Optional]
| Overrides the default mapmask behaviour of looking for any assignment
  or file with a likely name. The names correspond to logical
  assignments on the command line (e.g. ``MODE MSKIN MAPIN2``). This
  keyword is not compulsory but is recommended to explicitly ensure that
  the correct data is being used as input.

.. _print:

 PRINT MAP <axis> [ <first> [ <last> [ <step> ]]]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Print out map sections using the specified section axis. <axis> must be
one of X/Y/Z and specifies the section axis. <first>, <last> and <step>
specify the range of sections to print. The density values are output as
integers, and so this command will normally be used in conjunction with
the `SCALE <#scale>`__ card.

.. _mask:

 PRINT MASK <axis> [ <first> [ <last> [ <step> ]]]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Print out mask sections using the specified section axis. <axis> must be
one of X/Y/Z and specifies the section axis. <first>, <last> and <step>
specify the range of sections to print. Density values below the current
density cutoff will be marked as \`\*', density values above the cutoff
as \` '.

.. _scale:

 SCALE [FACTOR [c1 [c2]]]
~~~~~~~~~~~~~~~~~~~~~~~~~

Multiply the map by <c1> and add <c2>. Thus rho'=rho\*<c1>+<c2>. Default
<c1>=1.0, <c2>=0.0.

.. _scale:

 SCALE [SIGMA [c1 [c2]]]
~~~~~~~~~~~~~~~~~~~~~~~~

Scale the map to have a standard deviation of <c1> and a mean of <c2>.
Default <c1>=1.0, <c2>=0.0.

.. _scale:

 SCALE [MEAN [rhop [rhos]]]
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Scale the map so that the mean of the protein region is <rhop> and the
mean of the solvent region is <rhos>. This option attempts to put the
density on an absolute scale and depends on the low resolution terms
being used in the map calculation. MSKIN2 must be assigned. Default
<c1>=0.433, <c2>=0.330.

.. _scale:

 SCALE [RATIO [ratio]]
~~~~~~~~~~~~~~~~~~~~~~

Scale the map so that the standard deviation of the protein region is
<ratio> times the mean of the protein region. As the values of the
electron density are approximately normally distributed about the mean,
one can use standard tables of the area under a Gaussian curve. Thus
<ratio>=0.5 corresponds to approximately 31% of the grid points in the
protein being negative, <ratio>=1.0 corresponds to 16% being negative.
Default <ratio>=1.0.

.. _solv:

SOLV [FLIP <flipfac>] [ATTN <attnu>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Perform solvent flattening on MAPIN1 using the mask MSKIN2. If FLIP is
specified, the density shifts are multiplied by <flipfac>, thus
<flipfac>=1.0 corresponds to solvent flattening, <flipfac>=2.0
corresponds to solvent flipping. If ATTN is specified, then negative
density in the protein region is multiplied by <attnu>. <attnu>=0.0
corresponds to density truncation. Default <flipfac>=1.0, <attnu>=1.0.

.. _maps:

MAPS ADD/MULT
~~~~~~~~~~~~~

Combine maps by addition or multiplication. No scaling is performed.
MAP/MSKIN1 and MAP/MSKIN2 must be specified.

 EXAMPLES
---------

Simple unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`mapmask.exam <../examples/unix/runnable/mapmask.exam>`__ (Contains
several examples of how mapmask can be used).

....and non runnable examples in $CEXAM/unix/non-runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`mapcutting <../examples/unix/non-runnable/mapcutting>`__ (using
ncsmask, mapmask and overlapmap).

SEE ALSO
--------

`maprot <maprot.html>`__, `ncsmask <ncsmask.html>`__,
`sfall <sfall.html>`__, `bones2pdb <bones2pdb.html>`__.
