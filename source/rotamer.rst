ROTAMER (CCP4: Supported Program)
=================================

NAME
----

**rotamer** - list amino acids whose side chain torsion angles deviate
from the "Penultimate Rotamer Library"

SYNOPSIS
--------

| **rotamer xyzin** *foo\_in.pdb*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

| This program reads a protein coordinate file in PDB format and lists
  all amino acids whose side chain torsion angles deviate more than a
  user-defined threshold from the `rotamers <rotamer_table.html>`__ of
  the "Penultimate Rotamer Library" (`ref. 1 <#reference1>`__, Table I,
  "common-atom values").
| An amino acid is listed if at least one side chain torsion angle
  deviates more than a user-defined threshold `DELT <#deltachi>`__  from
  the equivalent one of the nearest rotamer. Empirically, the following
  two-step procedure to find the nearest rotamer seems to work quite
  reasonably, producing only a few false negatives or false positives:
  The first step is to choose a subset of potential nearest rotamers in
  a hierarchical order going from the first side chain torsion angle
  Chi(1) to the last side chain torsion angle Chi(n). For a side chain
  with four Chi-angles, first, all rotamers deviating less than the
  user-defined threshold in Chi(1) are chosen, then of those all
  rotamers that deviate less in Chi(2) are chosen, then of those all
  rotamers that deviate less in Chi(3) are chosen, and finally of those
  all rotamers that deviate less in Chi(4) are chosen. If a subset for
  Chi(i) is empty, the subset for the previous Chi(i-1) is chosen and
  the hierarchical search is terminated. If the subset for Chi(1) is
  empty, simply all rotamers are chosen as a "subset". The second step
  is to find the nearest rotamer out of the previously chosen subset of
  rotamers. Here, the nearest rotamer is the one with the minimum sum
  over the absolute Delta-Chi(i) values.

Some remarks and hints
^^^^^^^^^^^^^^^^^^^^^^

A reasonable threshold to start with is 30°, which is also the default.
In my experience, the deviation in Chi(1) is most important. So, watch
out for amino acids that deviate in Chi(1). For amino acids with longer
side chains, it is worth to look at residues with at least two torsion
angle deviations in the `printer output <#printer_output>`__. This could
be a hint that an alternative (rotameric) conformation might fit the
electron density as well. Remember, that even with current
maximum-likelihood refinement programs the electron density maps are
still somewhat biased towards the current model. So, even if an
alternative conformation doesn't fit the electron density map perfectly,
it might be worth to try a refinement of that conformation and inspect
the electron density map again. For proline residues, it would be
probably much better to check for possible ring pucker modes as done in
the program WHAT\_CHECK (`ref. 2 <#reference2>`__). Here, only the
Chi(1) value of proline residues is checked.

Known limitations:
^^^^^^^^^^^^^^^^^^

#. All necessary coordinates for calculating side chain torsion angles
   must be provided, otherwise the program issues a warning and skips
   the residue.
#. The output side chain torsion angles for residues with multiple side
   chain conformations depend on the order of the atoms: the last atoms
   in the coordinate file of all alternate conformations determine the
   values. For example, the following order of atom records in a valine
   residue with two alternate side chain conformations will result in
   the output of the side chain torsion angle of conformation B:

   | ATOM    650  CG1AVAL C 444       4.997  -1.309   3.806  0.60 19.67 
   | ATOM    651  CG1BVAL C 444       2.959  -1.564   5.384  0.40 29.48 
   | ATOM    652  CG2AVAL C 444       3.111  -1.499   5.347  0.60 12.83 
   | ATOM    653  CG2BVAL C 444       5.395  -2.257   5.300  0.40 12.68 

   whereas the following order of atom records will produce nonsense:

   | ATOM    651  CG1BVAL C 444       2.959  -1.564   5.384  0.40 29.48 
   | ATOM    650  CG1AVAL C 444       4.997  -1.309   3.806  0.60 19.67 
   | ATOM    652  CG2AVAL C 444       3.111  -1.499   5.347  0.60 12.83 
   | ATOM    653  CG2BVAL C 444       5.395  -2.257   5.300  0.40 12.68 

INPUT AND OUTPUT FILES
----------------------

 XYZIN
    Input coordinates in PDB format.

.. _keywords:

KEYWORDED INPUT
---------------

DELT <threshold>
~~~~~~~~~~~~~~~~

List all amino acids with at least one side chain torsion angle
deviating more than \|threshold\| from its nearest
`rotamer <rotamer_table.html>`__. If this keyword is not supplied or if
its value is outside the allowed range of [0,180]°, the default value of
30° is taken.

END
~~~

(Optional) Specifies the end of keyworded input and starts ROTAMER
running.

EXAMPLES
--------

List all amino acids with at least one side chain torsion angle
deviating more than 40° from its nearest rotamer:

::

      rotamer XYZIN foo_in.pdb << eof 
      delt 40
      end
      eof

Unix examples script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`rotamer.exam <../examples/unix/runnable/rotamer.exam>`__

PRINTER OUTPUT
--------------

Each amino acid is listed with its chain name, residue number, residue
name, a flag, and the observed Chi(i) values followed by the Chi(i)
values of the nearest `rotamer <rotamer_table.html>`__ in brackets. The
output flag has a "\*" in column "i" if the absolute value of
Delta-Chi(i) is greater than the user-defined threshold, and a "o"
otherwise. In the example below of a partially refined protein, the two
residues A444 Val and C450 Arg were intentionally modelled with wrong
side chain torsion angles into the electron density map. The residues
A461 Lys and B461 Lys are very poorly defined in the electron density
map.

Rotamer list with \|Delta-Chi\| = 30

|   Residue  Flag      Chi1         Chi2         Chi3         Chi4
| ===================================================================
| A 434 GLU  oo\*    -71 ( -67)   174 ( 180)   -74 ( -10)
| A 444 VAL  \*      104 ( 63)
| A 445 GLN  oo\*    -79 ( -67)   178 ( 180)   122 ( -25)
| A 446 LYS  ooo\*   -71 ( -67)  -179 ( 180)   178 ( 180)  -145 ( 180)
| A 448 GLN  oo\*    -80 ( -67)   174 ( 180)    96 ( -25)
| A 453 ARG  ooo\*   -73 ( -67)   171 ( 180)  -169 ( 180)  -143 ( 180)
| A 461 LYS  \*ooo  -146 (-177)    83 ( 68)   -177 ( 180)   179 ( 180)
| B 445 GLN  oo\*    178 (-177)   180 ( 180)  -121 ( 0)
| B 448 GLN  oo\*    -75 ( -67)  -177 ( 180)    83 ( -25)
| B 461 LYS  oo\*\*   -60 ( -67)  -176 ( 180)   148 ( 180)   145 ( 180)
| C 432 ARG  ooo\*   175 (-177)    71 ( 65)   -161 ( 180)   -88 ( 180)
| C 445 GLN  oo\*    -73 ( -67)  -177 ( 180)    62 ( -25)
| C 448 GLN  oo\*   -175 (-177)   161 ( 180)  -103 ( 0)
| C 450 ARG  o\*\*\*   -72 ( -67)  -135 ( 180)  -101 ( -65)   138 ( 105)

REFERENCES
----------

#. Lovell, S.C., Word, J.M., Richardson, J.S. & Richardson, D.C. "The
   penultimate rotamer library", Proteins: Structure, Function and
   Genetics, Vol.40, 389-408 (2000).
#. Hooft, R.W.W., Vriend, G., Sander, C. & Abola, E.E. "Errors in
   protein structures", Nature, Vol. 381, 272 (1996).

AUTHORS
-------

Dirk Kostrewa, Paul Scherrer Institute, Villigen, March 2002

SEE ALSO
--------

`Rotamer tables <rotamer_table.html>`__
