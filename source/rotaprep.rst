ROTAPREP (CCP4: Deprecated Program)
===================================

NAME
----

**rotaprep** - produces an MTZ file in multirecord form suitable for
input to SCALA.

SYNOPSIS
--------

| **rotaprep hklin** *foo.mtz* **hklout** *foo.mtz*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

**The rotaprep program has been replaced by `COMBAT <combat.html>`__.
Please use this alternative supported program. Rotaprep will be 'phased
out'.**

This program converts output from assorted data processing programs into
a `multirecord MTZ file <#files_output>`__ suitable for input to the
CCP4 scaling and merging programs (`SCALA <scale.html>`__ and
`TRUNCATE <truncate.html>`__). Only **mosflm** writes such an MTZ file.

Input types accepted at present are:
`**DATRED\_RIGAKU** <#datred_rigaku>`__, `**DENZO** <#denzo>`__,
`**JIMS** <#jims>`__, `**MTZF** <#mtzf>`__, `**MTZI** <#mtzi>`__,
`**MUF** <#muf>`__, `**MUI** <#mui>`__, `**RAXISDUMP** <#raxisdump>`__,
`**SAINT** <#saint>`__, `various **SCALEPACK** options <#scalepack>`__,
`**TEXHKL** <#texhkl>`__, `**TEXSAN** <#texsan>`__,
`**USER** <#user>`__, `**WEISS** <#weiss>`__,
`**XDSFULL** <#xdsfull>`__, `**XDSUNIQUE** <#xdsunique>`__.

Some of these are quite rare, but it seems sensible to keep them. It
would be trivial to extend the program for any format.

Warning: It is quite likely that the authors of processing programs will
change their formats without telling CCP4. If you have any queries or
knowledge of such changes please contact us.

Data Harvesting
^^^^^^^^^^^^^^^

`Data harvesting <harvesting.html>`__ (and some other functionality)
relies on the existence of appropriate project and dataset names, and
these should be assigned in ROTAPREP using the `PNAME <#pname>`__ and
`DNAME <#dname>`__ keywords. These will be written to the output MTZ
file, and be inherited by subsequent programs.

INPUT FORMATS
-------------

The following arguments to the `INPUT <#input>`__ keyword are supported.
The most commonly used arguments are those used to convert existing MTZ
files, or options for DENZO or SCALEPACK outputs (keywords
`**DENZO** <#denzo>`__, `**SCAL\_NM** <#scal_nm>`__ and
`**SCAL\_NM2** <#scal_nm2>`__). These are described first. The items in
the descriptions of the format have the following interpretations:

h k l
    Miller indices (possibly unique ones);
F sigF
    Measured structure factor amplitude and its standard deviation;
I sigI
    Measured intensity and its standard deviation;
delI sigdelI
    Anomalous intensity difference and its standard deviation;
ident / ibatch
    Batch number or equivalent;
mpart / fpart
    Partiality flag (in some convention).

MTZI
~~~~

Standard MTZ file containing h k l I sigI. It is possible to read in a
standard MTZ format file containing h k l I sigI and produce a
\`multirecord' MTZ file. You may wish to use a set of processed data as
a `reference \`batch' for SCALA <scala.html#run_reference>`__.

MTZF
~~~~

Standard MTZ file containing h k l F sigF. It is possible to read in a
standard MTZ format file containing h k l F sigF and produce a
\`multirecord' MTZ file. You may wish to use a set of processed data as
a `reference \`batch' for SCALA <scala.html#run_reference>`__.

DENZO
~~~~~

Concatenated output files from **DENZO**. These contain HEADER blocks
and reflection data. [``h k l fsqp I sigI fpart phi``] in format (3I4,
2X, 2F8.0, 7X, F6.0, 6X, 2F7.0, 14X, 2F6.0)

Care is needed when doing this, since no post-refinement has been done.
We now prefer to run **SCALEPACK** to refine the cell and orientation,
then *either* run DENZO again to re-integrate the images without
re-refining the cell *etc.* (however there may still be problems with
partiality flags), *or* output an unmerged file from scalepack (see
keywords `SCAL\_NM <#scal_nm>`__ and `SCAL\_NM2 <#scal_nm2>`__).

*N.B.* If using the commercialised Denzo identified like:

::

      HKL data processing system Version 1.0 FT1.31 Rev1.4

you need to use the Denzo command

::

      film york output file <filename>

to produce a suitable format for \`rotaprep'. The \`york' output can
also be read into \`scalepack' with

::

      format denzo_york1

SCAL\_NM
~~~~~~~~

Output from **SCALEPACK** option **NOMERGE no partials**
[``h k l ibatch isym I SigI (Fpart)``] in format (4i4,i3,2f8.0,f7.2).
Scales have been applied and partials summed. The Friedel pairs are
separate in this case.

SCAL\_NM2
~~~~~~~~~

Output from **SCALEPACK** option **NOMERGE original indices**
[``h k l h0 k0 l0 ibatch I SigI``] in format (6I4,i6,7x,2f8.0). Scales
have been applied and partials summed. The Friedel pairs are separate in
this case.

DATRED\_RIGAKU
~~~~~~~~~~~~~~

[``h k l F sigF``] in format (3i4,4f8.1).

JIMS
~~~~

| **MADNES for050** files.
| [``h k l I sigI``] in format (3i5,2f10.2).

MUI
~~~

| For *intensities* output from the **XENTRONICS** and **XENGEN** data
  processing packages.
| [format (3i4, 1x, F6.4, 6(1x,f8.2), 1x, i2).]

MUF
~~~

| For *amplitudes* output from the **XENTRONICS** and **XENGEN** data
  processing packages.
| [format (3i4, 1x, F6.4, 6(1x,f8.2), 1x, i2).]

RAXISDUMP
~~~~~~~~~

| Output from Raxis READBF of mergefile.
| [``mpart h k l I sigI ibatch``] in format (21x, I4, 4x, 3i4, 8x,
  2f10.3, i4).

Note: if you use the \`log' file produced under unix rather than the
\`output' file you may need to change the format to eat an extra space,
*i.e.* start it with \`22x'. Note also that you may find alternative
processing packages to be better than the Raxis software.

SAINT
~~~~~

Output from SAINT, one batch only (not fully tested).

SCALEPACK
~~~~~~~~~

Output from **SCALEPACK** [``h k l I+ SigI+ I- SigI-``] in format
(3i4,4f8.0).

*N.B.* This is not a very useful option. It is better to run
`**SCALEPACK2MTZ** <scalepack2mtz.html>`__ and
`**TRUNCATE** <truncate.html>`__ on the scalepack output, then use the
`MTZI <#mtzi>`__ option with the truncate output.

TEXSAN
~~~~~~

| Output from **TEXSAN** data processing package for Rigaku
  diffractometer.
| [``h k l ident I sigI``] in format (3i4, i3, 5x, f8.1, f7.1).

TEXHKL
~~~~~~

| Output from **TEXSAN** data processing package for Rigaku
  diffractometer.
| [``h k l I sigI scale``] in format (3x, 3i4, 2F10.2, 29x, F7.4).

USER
~~~~

ASCII data file in a user-defined format, see `LABEL <#label>`__ and
`FORMAT <#format>`__ keywords.

WEISS
~~~~~

Output from the **WEISS** data processing package for Japanese
Weissenberg data. [``h k l mpart I sigI ident``] in format (5x, 4i4,
2f8.2, i10).

XDSUNIQUE
~~~~~~~~~

| Output from Kabsch's **XDS** data processing package (or the MAR
  Research version) in the standard text file **UNIQUE.HKL** from the
  CORRECT stage. [The output of the **MARSCALE** program (and possibly
  the original **XSCALE**) can also be put in this form, although there
  is probably no reason to prefer these to `**scala** <scala.html>`__].
  This file contains symmetry averaged reflection intensities and
  anomalous differences after scaling.
| [``h k l I sigI delI sigdelI``] in format (3I5, 4E12.4)

Since UNIQUE output is scaled and merged, it is sensible to use
\`\ `f2mtz <f2mtz.html>`__' to produce an MTZ file for direct input to
\`\ `truncate <truncate.html>`__'.

XDSFULL
~~~~~~~

Output from Kabsch's **XDS** data processing package (or the MAR
Research version) in the standard direct access binary file **XDS.HKL**
from the CORRECT stage. This contains scaled intensities of all
reflections before symmetry averaging. Note that this file type will not
be generally portable between computer systems - run \`rotaprep' on the
system which generated xds.hkl.

DESCRIPTION OF THE OUTPUT MTZ FILE
----------------------------------

The output file has the column labels:

H K L
    Miller indices.
M/ISYM
    256\*M + ISYM, where the partiality flag M=0 for fully recorded
    reflexions and M=1 for partials, and ISYM is the symmetry number.
BATCH
    Batch (serial) number for this film/image.
I, sigI
    Corrected integrated intensity and its standard deviation.
FRACTIONCAL
    This is Calculated fraction recorded\*1000 (>500 if partial more
    than half present). For most inputs FRACTIONCAL is a dummy set to
    3000 or 501 for a partial.
RATDELSD
    1000\*ratio of worst delta(I)/STD.DEVN found *within* a film pack.
IPR, sigIPR
    is meant to be corrected profile-fitted intensity and SD. Here IPR
    and sigIPR are set equal to I and sigI if there is no appropriate
    information input.
XDET, YDET, ROT
    Detector position and PHI angle. This information is available for
    `DENZO <#denzo>`__ & `SAINT <#saint>`__ only.

.. _keywords:

KEYWORDED INPUT
---------------

Available keywords are:

    `ADDBATCH <#addbatch>`__, `BATCH <#batch>`__,
    `CELL <#cell>`__, `DETECTOR <#detector>`__,
    `DNAME <#dname>`__, `END <#end>`__,
    `FORMAT <#format>`__, `INPUT <#input>`__,
    `LABEL <#label>`__, `LABIN <#labin>`__,
    `MISBATCH <#misbatch>`__, `MONITOR <#monitor>`__,
    `PHIRANGE <#phirange>`__, `PNAME <#pname>`__,
    `REINDEX <#reindex>`__, `REJECT <#reject>`__,
    `RESOLUTION <#resolution>`__, `SCALE <#scale>`__,
    `SYMMETRY <#symmetry>`__, `TITLE <#title>`__

Compulsory input keywords for some options:
-------------------------------------------

.. _input:

INPUT <type>
~~~~~~~~~~~~

The allowed <type>s are those listed `above <#files_input>`__.

.. _batch:

BATCH <batch number>
~~~~~~~~~~~~~~~~~~~~

This specifies the \`batch' number for data in the multirecord MTZ
output. (BATCH is not required for DENZO, SCAL\_NM, SCAL\_NM2, SAINT or
USER input if a batch column is present in the input file. It is not
required for WEISS or TEXSAN inputs but if given it will override the
IDENT value.)

.. _cell:

CELL <a> <b> <c> [ <alpha> <beta> <gamma> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cell dimensions in Angstroems and angles in degrees; the angles default
to 90 degrees. This keyword is not compulsory for `MTZF <#mtzf>`__,
`MTZI <#mtzi>`__, `SCAL\_NM <#scal_nm>`__ or `DENZO <#denzo>`__ input,
but if given it will override the cell dimensions from the file headers.
This keyword is compulsory for other input types.

.. _labin:

LABIN <program label> = <file label> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| Only compulsory for `MTZF <#mtzf>`__ or `MTZI <#mtzi>`__ input.
| Column assignments for conventional labels F and SIGF are required for
  MTZF input, or for I and SIGI for MTZI input.

.. _label:

LABEL <file label 1> <file label 2> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Column assignments for user-defined input format (INPUT USER). <file
label n> must be chosen from H, K, L, M, BATCH, I, SIGI (or F, SIGF),
IPR, SIGIPR, FRACTION and given in the order in which they occur in the
input file. The indices H, K, L are compulsory. M is optional and
defaults to 0, *i.e.* fully recorded reflexions. The batch number should
either be read in from a BATCH column or taken from the BATCH keyword
(the former takes precedence if both are present). Either I, SIGI or F,
SIGF must be given; if the latter, then F is squared and I/SIGI output.
The remaining columns IPR, SIGIPR, FRACTION are optional.

.. _symmetry:

SYMMETRY <SG number> \| <SG symbol>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Specifies the spacegroup for the data. Compulsory if not
`MTZF <#mtzf>`__ or `MTZI <#mtzi>`__ input.

Optional input keywords:
------------------------

For input types which contain batch number information the following
three keywords determine the transformation of the input numbers:

.. _addbatch:

ADDBATCH <offset>
~~~~~~~~~~~~~~~~~

<offset> is added to all input batch numbers (default 0).

.. _misbatch:

MISBATCH <batch1> <batch2> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For inputs with batch information *e.g.* DENZO, then the number of batch
headers written to the MTZ file is from the maximum batch value to the
minimum. If some batches are missing, in this range, then use the
MISBATCH keyword to exclude them from the header. This keyword can only
be used once and is not for excluding batches from the data.

.. _phirange:

PHIRANGE <divisions>
~~~~~~~~~~~~~~~~~~~~

| (DENZO only, default 1.) Each batch is split into a number <divisions>
  of divisions for scaling. The output batch number will be:
| <divisions>\*(batch - 1) + phi\_step\_counter + <offset>

.. _format:

FORMAT <format string>
~~~~~~~~~~~~~~~~~~~~~~

Supply a format specifier for user-defined input (see `INPUT
USER <#user>`__). The argument must be a valid FORTRAN fixed format
string, such as might be given in a FORMAT statement, including the
brackets and quoted. *E.g.*:

::

       FORMAT '(6(6X,F8.3))'

will read records comprising six numbers each preceded by a
six-character-wide field which will be skipped. It is not possible to
read more than one reflexion from each input line. Make sure the format
is reading *real* numbers (F), and not integers (I)! If your input file
contains integers, read them with *e.g.* F6.0 rather than I6. Make use
of the X descriptor to skip unwanted columns. Under Unix, the cut (1)
command may be useful for reformatting the input columns if, for
instance, the relevant fields aren't in fixed positions. If the FORMAT
keyword is not given for user-defined input, then the default format
specifier '\*' is used.

.. _pname:

PNAME <project name>
~~~~~~~~~~~~~~~~~~~~

| [default: 'unknown']
| This keyword can be used to assign a project name for HKLOUT. For
  input options `MTZF <#mtzf>`__ and `MTZI <#mtzi>`__, a project name
  given by this keyword overrides any in HKLIN.
| This keyword is not compulsory, but is advisable to obtain a sensible
  name.
| A dataset, as listed in the MTZ header, is specified by a
  project-name/dataset-name pair. The project-name specifies a
  particular structure solution project, while the dataset-name
  specifies a particular dataset contributing to the structure solution.
  An entry in the PNAME keyword should therefore be accompanied by a
  corresponding entry in the `DNAME <#dname>`__ keyword.

.. _dname:

DNAME <dataset name>
~~~~~~~~~~~~~~~~~~~~

| [default: 'unknownddmmyy']
| This keyword can be used to assign a dataset name for HKLOUT. For
  input options `MTZF <#mtzf>`__ and `MTZI <#mtzi>`__, a dataset name
  given by this keyword overrides any in HKLIN.
| This keyword is not compulsory, but is advisable to obtain a sensible
  name rather than 'unknownddmmyy' (where ddmmyy is the date, with no
  spaces).
| A dataset, as listed in the MTZ header, is specified by a
  project-name/dataset-name pair. The project-name specifies a
  particular structure solution project, while the dataset-name
  specifies a particular dataset contributing to the structure solution.
  An entry in the DNAME keyword should therefore be accompanied by a
  corresponding entry in the `PNAME <#pname>`__ keyword.

.. _monitor:

MONITOR <nmon>
~~~~~~~~~~~~~~

Every <nmon>-th reflection is monitored (printed out). Default: no
monitoring.

.. _reject:

REJECT <hrej> <krej> <lrej>
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The arguments specify indices of reflections to be excluded from the
output file (these indices should lie in the spacegroup's chosen
asymmetric unit). All reflections equivalent to this set of indices will
be rejected. This is a desperate measure to weed out bad reflections
(usually overloads) without repeating the data processing. This line can
be repeated up to 200 times.

.. _reindex:

REINDEX <transformation>
~~~~~~~~~~~~~~~~~~~~~~~~

<transformation> specifies how to transform the original indices for the
output. It is specified in the form k,h,-l; h,-k,-h/2-1/2; etc. The
default is no transformation.

.. _resolution:

RESOLUTION <resmin> [ <resmax> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Rejects reflections outside the given resolution range in Ås or
4s\*\*2/lambda\*\*2.

.. _scale:

SCALE <scale>
~~~~~~~~~~~~~

The input intensities are multiplied by <scale> (default 1.0).

.. _title:

TITLE <title>
~~~~~~~~~~~~~

<title> is written to the output file.

.. _detector:

DETECTOR <xmin> <xmax> <ymin> <ymax>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

limits of detector coordinates XDET, YDET. These are needed for the
detector scaling options in `Scala <scala.html>`__.

.. _end:

END
~~~

to finish.

SEE ALSO
--------

`scala <scala.html>`__, `f2mtz <f2mtz.html>`__, `Data
Harvesting <harvesting.html>`__, `CAD <cad.html>`__, \`The marxds
marscale manual', MAR Research.

EXAMPLES
--------

::

    rotaprep HKLIN raxis.dump HKLOUT junk.mtz << +
    CELL 63 63 264 90 90 90
    MONITOR 1000 
    BATCH 1                # first batch number
    INPUT RAXISDUMP
    RESOLUTION 0 0.137
    SYMMETRY 89
    TITLE   Test rota_prep
    PNAME myproject
    DNAME native
    END 
    +

::

    /y/ccp4/cadd/junk/rotaprep                       \
    HKLIN ~rick/ESRF/pcrb/pcrb5_e1.sca \
    HKLOUT $SCRATCH/pcrb5_e1.mtz    \
    << 'END-mtzrwrot' 
    MONI 10000 
    INPUT SCAL_NM2
    ADDBATCH 0
    CELL 92.004  92.004 175.229  90.000  90.000  90.000
    SYMM P41212
    TITLE Test on Scalepack - NoMerge- Original Indices L1
    PNAME myproject
    DNAME native
    END 
    'END-mtzrwrot'
    #

AUTHOR
------

Eleanor Dodson, University of York
