FFT (CCP4: Supported Program)
=============================

NAME
----

**fft** - fast Fourier transform

SYNOPSIS
--------

| **fft HKLIN** *foo.mtz* **MAPOUT** *foo.map* [ **ABCOEFFS** *foo.txt*
  ]
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

The FFT (Fast Fourier Transform) program may be used to calculate
Fouriers, difference Fouriers, double difference Fouriers, Pattersons
and difference Pattersons from reflection data. The type of map is
controlled by the input flags, principally `LABIN <#labin>`__. The input
file will contain h k l Fi... SIGFi... Phases weights etc. and the
required Fourier coefficients will be generated from these.

 Only P1 is available in FFT. This is the old FFTBIG. This reduces
internal I/O, but you must have plenty of real memory to use FFT - it
will cause bad disc thrashing if the calculation won't fit into real
memory.

.. _keywords:

KEYWORDED INPUT
---------------

The various data control lines are identified by keywords. Only the
first 4 characters of a keyword are significant. Absent values default
to 0 unless otherwise stated. The cards can be in any order, except END
(if present) which must be last. Numbers in [ ] are optional and can be
left out. The only compulsory command is `LABIN <#labin>`__. The
available keywords are:

`AXIS <#axis>`__, `BIAS <#bias>`__, `BINMAPOUT <#binmapout>`__,
`EXCLUDE <#exclude>`__, `FFTSPACEGROUP <#fftspacegroup>`__,
`FREERFLAG <#freerflag>`__, `GRID <#grid>`__, `LABIN <#labin>`__,
`LIST <#list>`__, `LPMAP <#lpmap>`__, `LPFMT <#lpfmt>`__,
`NOCHECKS <#nochecks>`__, `PATTERSON <#patterson>`__,
`PHTRANSLATION <#phtranslation>`__, `PROJECTION <#projection>`__,
`RESOLUTION <#resolution>`__, `RHOLIM <#rholim>`__, `SCALE <#scale>`__,
`SYMMETRY <#symmetry>`__, `TITLE <#title>`__, `VF000 <#vf000>`__,
`XYZLIM <#xyzlim>`__, (END)

.. _labin:

LABIN <program label>=<file label>...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| (Compulsory.)
| This command defines which items are to be used in the calculation and
  these definitions largely control the type of map. *E.g.*

F1=FP SIG1=SIGFP PHI=PHIC

Names of items for the calculation are selected from the following list.
H, K, L will always be used.

A
    real part of structure factor
B
    imaginary part of structure factor
F1
    first F
SIG1
    standard deviation of first F
F2
    second F
SIG2
    standard deviation of second F
PHI
    phase angle in degrees
W
    reflection weight
W2
    weight for second F
PHI2
    phase for second F
FH
    heavy atom Fh
PHIH
    heavy atom phase
DANO
    anomalous difference for anomalous Fourier (see
    `below <#map_anom>`__)
I
    Intensity to be used for Patterson
SIGI
    standard deviation of Intensity
FREE
    The Free R flag. Reflections flagged with a free R value selected by
    the FREERFLAG keyword (default 0) will be omitted from the map.
    Don't assign this column if you don't want to omit data.

The items selected will determine the Fourier coefficients used for the
calculation. If any of the assigned columns contains a missing datum the
whole reflection will be omitted.

Common types of maps:

1) F1 [SIG1] PHI [W]
    The Fourier summation has:
    Amplitudes - [W] F1
    Phases - PHI
    The values of SIG1 may be used to exclude some terms; see
    `EXCLUDE <#exclude>`__.

Used for example for MIR Fobs maps - F1 = Fp, PHI=PHImir W=FOMmir; Fcalc
maps - F1=Fcalc PHI=PHIcalc

2) F1 [SIG1] F2 [SIG2] PHI [W]

The Fourier summation has:

    Amplitudes - [W] (SCALE1 F1 - SCALE2 F2)
    Phases - PHI
    The values of SIG1 and SIG2 may be used to exclude some terms; see
    `EXCLUDE <#exclude>`__. Also, they are used to check for missing
    reflections, see `below <#note_missing_reflections>`__.

Used for example for 2Fp-Fcalc maps:

    F1=Fp SIG1=SIGFp F2=Fcalc PHI=PHIcalc Scale1=2 SCALE2=1

Difference maps to find sites in a second derivative using phases
calculated from the first derivative. [W] (FPh2 - Fobs) maps:

    F1=FPH2 SIG1=SIGFPH2 F2=Fp SIG2=SIGFP PHI=PHIsir1 W=FOMsir1

3) DANO [SIG1] PHI [W]
    The Fourier summation has:
    Amplitudes - [W] (DANO)
    Phases - PHI - 90

Used for \`anomalous Fourier' maps. DANO is the column for the anomalous
amplitude, and the phase angle (PHI) is retarded by 90 degrees.

4) A B

| Some programs output values of A and B ready for summation, *e.g.*
  **SHELX**,\ **tffc**.
| The Fourier summation has:

    Amplitudes - sqrt (A \* A + B \* B)
    Phases - atan2 ( B,A)

5)
    More complicated vector difference terms can be generated. If you
    want to find further sites in a derivative you may want to calculate
    a \`[W] (FPHi - ( Fp+FH))' difference Fourier, *i.e.* a *double
    difference* Fourier. To do this assign
    F1 [SIG1] F2 [ SIG2] PHI [W] FH PHIH

6)
    Unspecified vector difference terms can be generated. To do this
    assign:
    F1 [SIG1] F2 [ SIG2] PHI [W] PHI2 [W2]

The map will sum:

    [W] SCALE1 F1 with phase PHI - [W2] SCALE2 F2 with phase PHI2

7)
    Some other keywords modulate the generation of terms. If PATTERSON
    is specified all amplitudes are squared (after all scaling, checking
    etc.) and phases set to 0. The symmetry of the spacegroup is used to
    generate the appropriate Patterson symmetry, and this is checked
    against the symmetry of the FFTSPACEGROUP. Note Pattersons should be
    calculated in P-1(2) P2/m(10) or Pmmm(47) Unspecified vector
    difference terms can be generated.
8)
    If PHTRANSLATION is specified the program uses:
    Amplitudes - [W] F1 F2
    Phases - PHI - PHI2
    If PHTRANSLATION HAND 2 is specified the program uses:
    Amplitudes - [W] F1 F2
    Phases - PHI + PHI2 + origin correction if required ( only for I41
    etc)

See `PHTRANSLATION <#phtranslation>`__. If the maximum peak in the
phased translation map is high (typically greater than 50 RMS), then for
hand 1 the model corresponding to PHI2 will need to be moved by this
amount to overlap the model corresponding to PHI. If HAND 2 is
specified, the model corresponding to PHI2 will need to be inverted then
moved.

\ ***N.B.* IMPORTANT!** The items SIG1 and SIG2 are used to check the
validity of the F for a reflection. This can be done in two ways at
present either by checking SIGFn=0 or by checking SIGFn=MNF (see
`documentation on Missing Number Flags <mtzmnf.html>`__). MNFs give a
more reliable way of checking reflections because even Fc and PHI can be
checked. Another benefit with MNFs is for certain types of map F1 can be
replaced by F2 if F1=MNF (see `FILLIN <#fillin>`__). Sigma cutoffs can
also be applied in order to reject reflections (see
`EXCLUDE <#exclude>`__).

All other keywords are given in alphabetical order below.

.. _axis:

AXIS <fast> <medium> <slow>
~~~~~~~~~~~~~~~~~~~~~~~~~~~

*In general DO NOT change the axis order unless you are in P1 or are
very sure you know what you are doing! This is permitted if your map is
being calculated using the routines for P1 or P222, but not otherwise.*

<fast> <medium> <slow> are the letters X, Y and Z in the appropriate
order. Note that many of the FFT space-group specific routines have
FIXED axis orders. In general these are Y,X,Z for those which have
rotation axes along c, and are best output with z-sections (all
space-groups with space-group number greater than 18 (P21212)).

For P1, the monoclinic space-groups, and space-groups 16 (P 2 2 2), 17
(P 2 2 21) and 18 (P 21 21 2), the required axis order is Z,X,Y, which
gives y-sections.

If this keyword is left out (the recommended procedure), and the map is
calculated in FFT space-group P1 (the FFTBIG default) then the standard
order for TRUE space-group is used, *i.e.* Z,X,Y for P1, the monoclinic
space-groups, and space-groups 16, 17 and 18; for all higher symmetry
space-groups the standard axis ordering will be Y,X,Z.

Otherwise, note that any axis permutation must make sense in the
space-group used for the FFT, *e.g.* the P21 FFT (sg4) always generates
a 21 axis along the section axis, through the origin. Axis permutation
is only permitted in a few space-groups and should probably by cyclic:
1, 2, 19, 21, 23, 47. In space-group 19, only cyclic permutations are
valid, *i.e.* Y,X,Z (default); Z,Y,X; or X,Z,Y; others will give WRONG
ANSWERS.

.. _bias:

BIAS <bias>
~~~~~~~~~~~

| If <bias> .NE.0, F = (F\*\*3)/(F\*\*2+(<bias>\*SD(F))\*\*2)
| This is a bias correction for anomalous Patterson maps due to Ian
  Tickle (perhaps useful, perhaps not).

.. _binmapout:

BINMAPOUT [ NONE ]
~~~~~~~~~~~~~~~~~~

Send binary output to MAPOUT. This is the default. NONE stops this.

.. _exclude:

EXCLUDE <keyword> <value> ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Each secondary keyword is followed by a number setting the appropriate
limit.

::

             SIG1 Nsig1   and SIG2 Nsig2
             *** reflections excluded if |F1| lt Nsig1 * sd(F1) 
                                      or |F2| lt Nsig2 * sd(F2)

             F1LIM (F1minimum)     F2LIM (F2minimum) IMIN (Minimum I)
             *** reflections excluded if |F1| (or |DANO|) lt F1minimum 
                                      or |F2| lt F2minimum
                                      or I  lt Iminimum
                  (cut off applied BEFORE scaling or squaring for Patterson)

             F1MAX F1maximum   F2MAX F2maximum  IMAX Imaximum
             *** reflections excluded if |F1| (or |DANO|) gt F1maximum
                                      or |F2| gt F2maximum
                                      or I  gt Imaximum
                  (cut off applied BEFORE scaling or squaring for Patterson)

             DIFF difference_limit
             *** reflections excluded if 
                 Abs(Sc1*F1 - Sc2*F2) gt difference_limit
                  (cut off applied BEFORE squaring for Patterson)

             TERM limit
             *** reflections excluded if 
                  Sqrt( A**2 + B**2) gt limit for Fourier
                       A**2 or F**2  gt limit for Patterson

             [default for all =  0.0 == no cut off]
         

It is very important to use this sensibly for difference or anomalous
Pattersons. As the terms in a Patterson are squared the map can be
distorted by one or two large \`differences' which have been
overestimated. It is sensible to use the SIGi cut offs to exclude weak
data, and to inspect the SCALEIT output to decide on a sensible value
for DIFF. Many instruments have glitches and you may have a few
completely incorrect estimates of F(hkl) which can hide the true signal.

When calculating \`anomalous Pattersons', internally DANO is treated
like F1. Therefore, it is possible to assign F2 and SIG2 to the
appropriate FPH and set SCALE F2 to 0.0001 or something extremely small.
Formally you will be calculating a difference Patterson (F1-F2) but
because F2 is very small you are left with just the F1/DANO term. The
advantage is that you can use the SIG2 check to exclude anomalous
differences from weak measurements.

If the input option to input intensities (LABI I= ...) is used, an
origin removed Patterson can be calculated.

The terms required for calculating an origin removed Patterson. F2OR =
F\*\*2 -<E\*\*2> and E2OR = E\*\*2 - <E\*\*2> = E\*\*2 - 1.0. They can
be used as input to the fft programs using LABI I=F2OR, etc ( See ecalc
documentation)

.. _fftspacegroup:

FFTSPACEGROUP <SG>
~~~~~~~~~~~~~~~~~~

Space group name or number for Fourier calculation. Possible values
[<name> (<number>)] are:

P1 (1), P1bar (2), P21 (4), P2/m (10), P21212 (18 and 1018), P212121
(19), C2221 (20), C222 (21), I222 (23), Pmmm (47), P41212 (92), P43212
(96), P31 (144)/P32 (145), R3 (146), P3121 (152), P3221 (154), P61
(169), P65 (170).

For a Patterson the space group should be P1bar(2) (for all space
groups), P2/m(10) (for all monoclinic space groups, b axis unique), or
Pmmm(47) (for all orthorhombic space groups).

If this command is omitted, the true symmetry is used if it is one of
the FFT spacegroups, otherwise spacegroup P1 or P-1 is used. If the
crystal symmetry is different to this the program will check that the
symmetry operators are compatible. An (incomplete) list of possible
symmetry pairings is given `below <#spgp_symm_pair>`__. For further
examples consult International Tables Vol. A.

FILLIN
~~~~~~

When this keyword is specified the Fourier term

::

        k1 * F1 * exp{B1*s}   -   k2 * F2 * exp{B2*s}

is replaced by this term below if F1=MNF

::

        [k1 * exp{B1*s}  -  k2 * exp{B2*s}] F2.

Where k1,k2 and B1,B2 are the scales and Bfactors specified by the user
respectively. The types of maps this procedure would benefit are 2Fo-Fc,
3Fo-2fc ... (n+1)Fo-nFc maps. Noise levels could be reduced because
rather than treating each Fourier term as zero for unmeasured Fo
reflections, it will given an approximately correct value (Fc). However,
the down side in these maps is that model bias would certainly increase.
Maps should not be calculated beyond the resolution of the observed data
for obvious reasons.

.. _freerflag:

FREERFLAG <freeflag>
~~~~~~~~~~~~~~~~~~~~

Reflections where the value of the column FREE, taken from the MTZ file,
equals <freeflag> (default 0) are omitted from the calculation. If you
do not wish a free-R set to be used don't assign FREE.

.. _grid:

GRID <nx> <ny> <nz> \| SAMPLE <grid\_sample>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Number of sampling divisions along whole cell edge (see `restrictions
below <#grid_restr>`__). Alternatively, the keyword SAMPLE allows
specification of the fineness of sampling, *e.g.* GRID SAMPLE 3 gives a
grid of about 1/3 of the maximum resolution: this is the default if this
command is omitted. A larger value gives a finer sampling (minimum
sampling = 2).

.. _list:

LIST
~~~~

List Fourier terms in ASCII to logical name ABCOEFFS. This may be useful
if you want to monitor the input, but the output file is HUGE and it
should normally be suppressed.

The columns output are:

::

       H  K  L  k1*F1  PHI  k2*F2  PHI2  W1  W2   A  B  ATAN(B,A) SQRT(AA+BB)

See the `LABIN <#labin>`__ keyword for a description of the symbols.

If, for instance, your Pattersons are junk, and you suspect some special
problem - all 0 2k 4l are junk or something - you can sort it on the
SQRT(AA+BB) column to see where the large contributions come from. You
see sometimes all F2 for these are <1 or something and get immediately
suspicious.

.. _lpmap:

LPMAP
~~~~~

Output map to printer.

.. _lpfmt:

LPFMT <nlines> <ncol> <idig> <ilin> <max> <min>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For printer output, change format from default.

    <nlines>
        number of lines/page (if=0, reset to NR)
    <ncol>
        number of columns/page (if=0, reset to maximum)
    <idig>
        number of digits in output numbers to LP (default=4)
    <ilin>
        number of lines between output lines of map (default=1)
    <min>,<max>
        resets rho to 0 if <min><rho<<max>. (Can be used *e.g.* to set
        negative regions to 0 <min> = -9999, <max>=0.)

.. _nochecks:

NOCHECKS
~~~~~~~~

Specifies that no symmetry expansion or permutation is required and no
checks needed to see if h k l is in the correct asymmetric unit.

| Limits on the value of h,k,l:
| These limits are required by the program if NOCHECK is used They are
  those used throughout the CCP4 suite to cover a single asymmetric unit
  of reciprocal space.

FFT symmetry
Triclinic
Monoclinic
Orthorhombic
    hkl: h>=0, k>=0 and l>=0
Tetragonal
    pg4 hkl:h>=0, l>=0 with k>=0 if h=0 and k>0 if h>0
    pg422 hkl:h>=0, k>=0, k<=h, l>=0
Trigonal
Cubic
    pg23 hkl:h>=0, k>=0, l>=0 with l>=h, k>=h
    pg432 hkl:h>=0, k>=0, l>=0 with k>=l

If the FFTSPACEGROUP is different to the true SYMMETRY or the AXIS order
is non-standard for the FFT spacegroup the NOCHECK option is overridden.

.. _patterson:

PATTERSON
~~~~~~~~~

Calculate a Patterson map (Fourier is the default). If F1 is assigned on
the LABIN line the amplitudes are squared after scaling and taking any
differences. If I is assigned the input term is used as read. This
allows you to calculate origin removed pattersons using the E2OR and
F2OR terms output by ECALC.

.. _phtranslation:

PHTRANSLATION [ HAND 2]
~~~~~~~~~~~~~~~~~~~~~~~

| Calculate a phased translation function (`reference
  [3] <#reference3>`__). The terms are:
| w\*F1\*F2\*exp(i(PHI - PHI2))
| If the "change hand" flag is set ( HAND 2) the terms are
| w\*F1\*F2\*exp(i(PHI + PHI2 +CX \*IH +CY \*IK +CL\*IL ))
| For most space groups CX=CY=CL = 0.0 , ie the phases for the opposite
  hand is -PHIhkl. However the space groups I41, I4122, F4132, and
  I4132, changing hand also implies a change of origin. For these space
  groups the following correction is required.

::

     
          Space_group CX    CY    CZ
              I41    0.0   0.5   0.0
             I4122   0.0   0.5   0.25
             F4132   0.75  0.25  0.75
             I4132   0.25  0.25  0.25

Scale is multiplied by 1/sqrt(Sigma((w\*F1)\*\*2)\*Sigma((F2)\*\*2)

If the maximum peak in the phased translation map is high (typically
greater than 50 RMS), then for hand 1 the model corresponding to PHI2
will need to be moved by this amount to overlap the model corresponding
to PHI. If HAND 2 is specified the model corresponding to PHI2 will need
to be inverted then moved.

.. _projection:

PROJECTION
~~~~~~~~~~

Calculate projection along section axis. All reflections except those in
the zone are ignored. The sampling interval along the section axis
`GRID <#grid>`__ should be left unchanged from 3D value, and the map
limits on the section axis `XYZLIM <#xyzlim>`__ will be reset to 0 0.

.. _resolution:

RESOLUTION <rmin> <rmax>
~~~~~~~~~~~~~~~~~~~~~~~~

| <rmin> and <rmax> are resolution limits in Angstrom (either order). If
  one is 0 or blank, no high resolution cutoff, if both blank, no
  cutoff.
| If this command is absent, the default is to use all reflections in
  the file. For translation functions you MUST omit this command, to
  allow the program to accept all reflections in the file.

.. _rholim:

RHOLIM <rhomax> [ <rhomin> ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The scaling of the output map is usually controlled by the values input
for VF000. However If these are set, the map will be automatically
scaled (using v and F000) to give a maximum <rhomax> (and minimum if
<rhomin> is given) in the first group of sections. The default option
(which should normally be used, except possibly for Pattersons) gives a
map on the true scale.

.. _scale:

SCALE F1 \| F2 <scale> <B-factor>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| *e.g.* SCALE F1 2.0 0.0 F2 1.0 0.0
| Read scale and temperature factor to be applied to amplitude (F1 or
  F2). Default Scale = 1.0, B-factor = 0.0. Scales are also applied to
  the associated SIGi.
| Fused = <scale> \* F \* exp(-<B-factor> (sin theta/lambda)\*\*2)

SYMMETRY <SG>
~~~~~~~~~~~~~

Specifies true space-group as a name, number or explicit operators in
the standard form. By default, this is taken from the input file, and it
will probably cause chaos if you set it to anything else.

TITLE <title>
~~~~~~~~~~~~~

80 character title for map - sensible to choose an informative one.

VF000 <vol> <F000>
~~~~~~~~~~~~~~~~~~

These parameters can be used to modify the scaling of the output map.
The default values are <vol>=Cell\_volume, and <F000>=0.0 These give a
map approximately on absolute scale, and should usually not be altered.
Setting <vol> = Cell\_volume/100 would inflate the map values by 100.

.. _xyzlim:

XYZLIM <xmin> <xmax> <ymin> <ymax> <zmin> <zmax> \| ASU
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Range of cell to be calculated. This defines the extent of the output
map. The arguments to the XYZLIM keyword can be either: (i) six numbers
giving the limits in cell divisions OR fractional coordinates, or else
(ii) the subkeyword ASU (which will automatically set the map limits to
be the CCP4 asymmetric unit for the true or Patterson spacegroup, as
appropriate). See `below <#section_restr>`__ for minimum and maximum
requirements for each spacegroup.

It is essential that <xmin> greater or equal to 0, and <xmax> is less
than NX and so on (see definition of `GRID <#grid>`__). For other
restrictions, see `below <#restrictions>`__. Some limits can be reset by
the program but not all. In particular if your grid disagrees with these
limits the program will not necessarily reset them sensibly.

If this command is omitted, the limits will default to those of the
asymmetric unit for the "fft space-group" - *i.e.* the spacegroup in
which the fft calculation is performed in. This may or may not be the
same as the asymmetric unit of the true spacegroup.

For many spacegroups there are several choices of such an asymmetric
unit, and you may wish to override the default one. The inverse
transform used in `SFALL <sfall.html>`__ has quite strict limits, and if
you want to use that you probably ought to take the default values,
which are compatible. Remember that if the map is to be extended to
cover a molecule, it is fastest to calculate as much of the cell as
possible (the whole cell in P1) as input to EXTEND.

RESTRICTIONS
------------

Grid restrictions:
~~~~~~~~~~~~~~~~~~

Note that symmetry expansion is not permitted to space-groups marked by
\*. For all space-groups, NX,NY,NZ must have no prime factors greater
than 19, and NX must be greater than 2 HMAX + 1, etc. Below, \`n' is an
integer, so \`2n' means the number must be even.

::

            Space              section
            group      number    axis

            P1            1       y   NZ=2n 
            P-1           2       y   NY=2n, NZ=2n, NX=2n, 
            P21           4       y   NY=4n, NZ=2n, 
            P2/M         10       y   NX=2n, NY=8n, NZ=2n, 
           *P21212       18       y   NY=4n, NZ=2n, 
           *P21212a    1018       y   NY=4n, NZ=2n, 
            P212121      19       z   NY=2n, NZ=4n, 
           *C2221        20       y   NX=4n, NY=4n, NZ=4n, 
           *C222         21       y   NX=4n, NY=4n, NZ=4n, 
            I222         23       y   NX=4n, NY=4n, NX=4n, 
            PMMM         47       z   NX=8n, NY=8n, NZ=8n, 
      *P41212/P43212  92,96       z   NX=2n, NY=NX, NZ=8n, 
      *P31/P32      144,145       z   NX=6n, NY=6n, NZ=6n  
      *R3               146       z   NX=6n, NY=6n, NZ=6n 
      *P3121/P3221  152,154       z   NX=6n, NY=6n, NZ=6n
      *P61/P65      169/170       z   NX=6n, NY=6n, NZ=12n

Section restrictions:
~~~~~~~~~~~~~~~~~~~~~

::

             Spacegroup           Section limits

                P1      all Z  all X      Y      0 to NY-1   
                P1bar   all Z  all X      Y      0 to NY/2-1 
                P21     all Z  all X      Y      0 to NY/2-1 
                P2/m  0-1/2 X  all Z      Y      0 to NY/2-1
                P21212  all Z  all X      Y      0 to NY/2    
                P21212a all Z  all X      Y      0 to NY/2    
                P212121 all X  all Y      Z      0 to NZ/2    
                C2221 0-1/2 Z  all X      Y      0 to NY/4   
                C222  0-1/2 Z  all X      Y      0 to NY/4   
                I222  0-1/2 Z  all X      Y      0 to NY/4   
                Pmmm  0-1/2 X 0-1/2 Y     Z      0 to NZ/2  
        ??      P4122   all X  all Y      Z      0 to NZ/2    
                P41212  all X  all Y      Z      0 to NZ/2    
        ??      P4322   all X  all Y      Z      0 to NZ/2    
                P43212  all X  all Y      Z      0 to NZ/2    
                P31     all X  all Y      Z      0 to NZ-1   
                P32     all X  all Y      Z      0 to NZ-1   
                R3      all X  all Y      Z      0 to NZ/3-1 
            or  R3    0-1/3  X and Y      Z      0 to NZ -1 
                P3121   all X  all Y      Z      0 to NZ-1   
                P3221   all X  all Y      Z      0 to NZ/6    
                P61     all X  all Y      Z      0 to NZ/6-1 
                P65     all X  all Y      Z      0 to NZ/6-1 

| For maps with Y-sections the output has Z fastest and X medium.
| For maps with Z-sections the output has Y fastest and X medium.

Possible FFTSPACEGROUP/SYMMETRY pairing:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         
            FFTSPACEGROUP             Crystal spacegroup
            group            number  

            P1                 1       all
            P-1                2       all centric spacegroups, Pattersons
            P21                4       C21 I21 P21212a P...
            P2/M              10       Pmmm etc
           *P21212            18       C222  F222  I222
           *P21212a         1018       C222a F222a I222a
            P212121           19       C2221a I212121  P42  cubics
            PMMM              47       P4/mmm
           *P41212/P43212  92,96
           *P31/P32      144,145
            R3               146       R32
           *P3121/P3221  152,154
           *P61/P65      169/170       P6122/P6522

INPUT AND OUTPUT FILES
----------------------

The input files are the control data file and a standard MTZ reflection
data file.

Unless suppressed, a standard format output map file will be written by
the program.

Input:
~~~~~~

    HKLIN
        input data
    SYMOP
        symmetry operation library

Output:
~~~~~~~

    MAPOUT
        output binary map
    ABCOEFFS
        Output unit for an optional listing of the generated Fourier
        coefficients (for diagnostic purposes). See the `LIST <#list>`__
        keyword.

PRINTER OUTPUT
--------------

The printed output starts with details from the input control data and
details from the MTZ subroutines when the input reflection data file is
read. Details are given of the array sizes and of the scratch files
used. If a line printer listing of the map was requested, each section
is listed in the requested format preceded by a title and details
identifying the section and followed by the minimum and maximum density
(E) values on the section. If no line printer listing was requested then
only the minimum and maximum densities for each section.

Error Messages are MEANT to be self explanatory, here are some of them:

You may get errors from binsort which is in fact a separate program

The FFT uses a Buffer set in the Main program. If you get the BUFFER Too
Small message you can either: reduce your grid size, or recompile the
program with a bigger value for IDIM (currently 500000). Similar action
is needed if you get this message:

Array SIZE ?????? TOO Small

There are a series of error messages which mean your grid sampling does
not satisfy the requirements.

Prime factor > 19????

NX NY or NZ not divisible by correct factor. Check documentation.

Similar messages about grid restrictions - check documentation.

If NOCHECKS is set you must have reflections in right asymmetric unit,
and sorted. Run CAD to do this if you have a problem. You will get this
sort of error message.

Reflection out of Order. HKL = ?? ?? ?? K greater than H ?? ?? ??

Array Z too short in routine PRTSHF
    An obscure error - redimension it.

PROGRAM FUNCTION
----------------

The crystallographic fast Fourier transform (FFT) program was written by
Lynn F. Ten Eyck. The use of the FFT in crystallography has been
discussed by A. Immirzi (ref.1) and the methods for incorporating
crystallographic symmetry into FFT calculations has been discussed by
L.F. Ten Eyck (ref.2).

The computational speed of the FFT is due to two factors. An examination
of the algebra on which the Cooley-Tukey Fast Fourier Transform is based
has led to a different approach towards incorporating crystallographic
symmetry into Fourier transforms than that based on the trigonometric
relationships as given in the International Tables for Crystallography.
The method involves extracting the desired information from the results
of an ordinary complex Fourier transform of a short sequence rather than
a long one. Second, the complex Fourier transforms are calculated by an
extremely efficient and general set of subroutines for calculating one
dimensional transforms. The program does not suffer from any stringent
restrictions on the number of input points though certain rules have to
be obeyed as described above in notes 5 and 6.

The program has been developed by Birkbeck College to allow for the
generation of equivalent reflections using the space group symmetry
elements so that the program may be used for space groups other than
those for which it was specifically written.

It has also been developed to allow for the input of selected data items
from a binary reflection data file so that a number of different types
of Fourier transforms may be calculated without having to prepare a
special input file. These include: Fourier synthesis, difference
Fourier, double difference Fourier, Patterson function, and difference
Patterson functions.

If the reflection file has been modified to correctly include Missing
Number Flags then it is possible to substitute data. When calculating a
difference map of some kind *i.e.* using F1 and F2, then if F1=MNF then
F2 is substituted for F1. This is useful in reducing the noise in maps
for 2Fo-Fc or 3Fo-2Fc maps for instance. Low resolution terms tend to be
absent from measured data but contribute significantly to the overall
quality of the map. Assuming that these low resolution terms are zero
just because they are unmeasured can lead to significant noise in the
map. Therefore, substituting Fc for Fo will inevitably be a better
approximation than zero. The down side is that model bias will increase
in the map and may cause trouble if large sections of data are
unmeasured.

REFERENCES
----------

#. A.Immirzi, *Crystallographic Computing Techniques*, ed. F.R.Ahmed,
   Munksgaard, p399, (1966).
#. L.F.Ten Eyck, *Acta Cryst.*, **A29**, 183, (1973).
#. R.J.Read and A.J.Schierbeek *J. Appl. Cryst.* **21** 490-495 (1988).

EXAMPLES
--------

Simple unix example script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`fft.exam <../examples/unix/runnable/fft.exam>`__ (Example of the
calculation of difference patterson and anomalous difference patterson).

Also found combined with other programs in the example scripts ($CEXAM/unix/runnable/)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`mapcorrelation\_procedure <../examples/unix/runnable/mapcorrelation_procedures>`__
(Use of fft in calculations leading up to map correlations).

`tffc\_procedure <../examples/unix/runnable/tffc_procedure>`__ (Run FFT
in Space group 1).

`rsps.exam <../examples/unix/runnable/rsps.exam>`__ (Example of
calculating and interpreting Patterson).

`solomon.exam <../examples/unix/runnable/solomon.exam>`__ (Calculate
initial map with best phases and FOM weighted amplitudes).

`vecref.exam <../examples/unix/runnable/vecref.exam>`__ (Use in vector
space heavy atom refinement).

`patterson <../examples/unix/runnable/patterson>`__ (Patterson Example).

`dm.exam <../examples/unix/runnable/dm.exam>`__ (Calculate maps from
density modified map).

`mir\_steps <../examples/unix/runnable/mir_steps>`__ (Heavy atom
refinement and Phasing tutorial).

`3fo2fcmap <../examples/unix/runnable/3fo2fcmap>`__ (3Fo-2Fc map).

`fofcmap <../examples/unix/runnable/fofcmap>`__ (Calculate an Fo-Fc map
and find position of significant peaks).

`phased\_translation\_calc <../examples/unix/runnable/phased_translation_calc>`__
(Phased translation function for isomorphous phases set).

`waterpeaks <../examples/unix/runnable/waterpeaks>`__ (Procedure for
finding water peaks).

`watpeak.exam <../examples/unix/runnable/watpeak.exam>`__ (Procedure for
finding water peaks).

AUTHOR
------

Originator: Lynn F. Ten Eyck

SEE ALSO
--------

`sfall <sfall.html>`__
