IPDISP (CCP4: Deprecated Program)
=================================

NAME
----

**ipdisp** - X-Windows tool, displays images from a variety of
(crystallographic) sources.

SYNOPSIS
--------

**ipdisp** [`option <#options>`__]

DESCRIPTION
-----------

Use `idiffdisp <idiffdisp.html>`__ instead.

This program allows the display of images from a variety of sources. The
primary assumption is that the image file consists either of a known
(currently MAR) format, or of an array of pixel values expressed as
unsigned short integers (integer\*2), with an optional header (which is
ignored except for MAR images): this second class includes unpacked
images from most detector systems. The image file is described in a
so-called spdfil, which is specified on the command line.

Historically, this program was a stand-alone version of a display level
from Madnes, so follows a few Madnes ideas.

The program is run by

::


            ipdisp  [option]

    Examples:
            ipdisp my_image.mar2000

            ipdisp -Mo -f image.img

            ipdisp -M -t image_###.img -d 120 -w 0.88

The first [option] defines the appropriate image type, and may be
followed by options to set an image filename and to set parameters which
may otherwise be set interactively from the screen menu. In the case of
Mar IP files with extensions of the form ".marXXXX" (where XXXX is a
number eg 2000), the filename name itself (or template name following
-t) is sufficient

Contents:
~~~~~~~~~

#. `Options for ipdisp <#options>`__
#. `Description of the display window <#display_window>`__
#. `Details of the display window <#details>`__

   #. `Display parameter area <#display_parameters>`__
   #. `Output area <#output_area>`__
   #. `Main menu <#main_menu>`__
   #. `Image display area <#image_display>`__

#. `Description of spatial distortion files
   (spdfils) <#descript_spdfils>`__

   #. `Description of data items <#data_items>`__
   #. `Creating or customising spdfils <#create_spdfil>`__

#. `List of distributed spdfils <#list_of_spdfils>`__

Options for ipdisp
------------------

Optional input (as alternative to interactive input):

::


          -h    gives a list of the  image type options
          -s    defines spdfil; not needed if one of the above options is given
          -f    image filename: if this is given, it will be displayed immediately
          -t    template name
          -i    image increment (nearly always = 1,the default)
          -d    detector distance (mm)
          -a    swing angle theta (degrees)
          -w    wavelength (A)

If the filename or template name have extensions of the form ".marXXXX",
the type will be set automatically and the distance and wavelength will
be read from the header.

The following define the image type (ie which spdfil to read) - if used,
they MUST come before any other option: either version may be used for
unix, only the second for VMS ( in which case only the first two
characters are needed):

::


          -ab -AB      2x2 ADSC scanned binned mode
          -a  -A       2x2 ADSC scanned
          -m  -MAr     MAR  scanner (small plate, SGI)
          -mc -MC      MAR  CCD scanner 130mm
          -md -MD      MAR  CCD scanner 165mm
          -M  -BMar    MAR  scanner (large plate, SGI, squashed data)
          -Mo -BOmar   MAR  scanner (large plate, SGI, original format with overflow)
          -V  -MVaxmar MAR  scanner (large plate, Vax)
          -v  -MVax    MAR  scanner (small plate, Vax)
          -r  -R       RAXIS II scanner
          -rc -RC      RAXIS II scanner (coarse scan)
          -r4 -R4      RAXIS 4 scanner (100 micron scan)
          -pf -PF      Photon Factory Weissenberg plate
          -c  -C       Mac Science 2500x2500
          -d3 -D3      MAC Science DIP2030
          -d4 -D4      MAC Science DIP2040
          -dc -DC      D2AM CCD scanner

    [Obsolete?
          -e  -EMbl    EMBL scanner (prototype) 
          -g  -GEl     gel
          -o  -OLmb    Optronics film scan (LMB scan size)
          -O  -OOlmb   Optronics film scan (LMB scan size, original file)
          -y  -Ylmb    Molecular Dynamics off-line scanner at LMB
          -Y  -BYlmb   Molecular Dynamics large scanner at LMB
          -z  -XLmb    Molecular Dynamics small scanner at LMB
          -i2 -TIffgel TIFF file translated from a GEL file]

          or the spdfil filename can be given (full path not needed).

The complete list of supported detectors and their associated spdfils is
given below (see `"List of distributed spdfils" <#list_of_spdfils>`__)
(see the ipdisp script or COM file on VMS for details)

DISPLAY WINDOW
--------------

The display window is divided into 4 main areas: `Display Parameter
area <#display_parameters>`__, `Output area <#output_area>`__, `Main
menu <#main_menu>`__, `Image display area <#image_display>`__.

The left mouse button is the main pointer, the middle button is used on
the image area to control the magnify window, the right button for
sub-menu options in the image area

 1) `Display parameters <#display_parameters>`__
    This allows a number of parameters affecting the display & other
    options to be changed. Yes/No toggles can be changed by clicking the
    left mouse button on the Yes/No button. Numbers can be changed by
    clicking on the number with the left button, then typing the number,
    terminated by <cr>
 2) `Output area <#output_area>`__
    This area reports the results of clicking on the image, etc. No
    input is accepted
 3) `Main menu <#main_menu>`__
    Click left button to select an option
 4) `Image display area <#image_display>`__
    In the window area, the left mouse button can be used to select a
    rectangle by dragging. The average value, rms deviation & number of
    points are reported in the `Output area <#output_area>`__. A
    selected rectangle may be zoomed to fill the display area by the
    menu option Zoom: click Zoom again to restore the full image.

The middle mouse button controls the magnify window (top left). Clicking
the middle button on the magnify window toggles a double size display.
Clicking the middle button on the image display freezes the magnify
window, allowing pixels in the magnify window to be picked (but not
rectangles).

The top right panel contains:-

#. Min & Max image values for the grey level scaling (equivalent to
   `Scale low & high <#display_scale>`__ in the parameter area, but less
   permanent)
#. cursor position in pixels (REVERSED in order, *i.e.* x==Zms, y==Yms).
   In zoomed images, this is the pixel position in the ZOOMED image, not
   in the whole image. See output area after clicking for `correct pixel
   values in the whole image <#output_last_pixel>`__.
#. Overlay On/Off/Offset menu, toggles display of circle and spot
   overlays (right button to select)
#. Contrast slider: controls display contrast (left button)
#. Colour table selection: probably only Black on white & White on black
   are useful (*N.B.* "Yellow if >Max" option makes pixels yellow if
   they are greater than scale-high (=Max) value)
#. Mag menu: controls magnification in magnify window
#. PS button: sends image to a Postscript file (after a little dialogue)

DETAILS
-------

1) Display parameter area
~~~~~~~~~~~~~~~~~~~~~~~~~

#. Signed image: raw images are Unsigned (=No, default). Selecting Yes
   will set negatives = 0 for the displayed image (but picked values are
   still correct)
#. Scale low, high: define scaling range, *i.e.* the values of an image
   pixel to be displayed as white or black. The first image displayed
   will be scaled automatically, and the range may also be recalculated
   with the Scale image option in the menu, but you may wish to change
   this. These numbers are exactly equivalent to the Min/Max numbers on
   the image panel, but numbers set here will carry over to all images.
   Use `Current image <#main_current>`__ option to redisplay image with
   altered scaling.
#. Pick area: controls number of pixels displayed by
   `Pick <#main_pick>`__
#. Whole range: not relevant at present
#. Add: controls the `Add images <#main_add_images>`__ & `Overlay
   images <#main_overlay_images>`__ options, but these parameters are
   also prompted for by these options. Define first image number &
   number of images to add together or overlay.
#. Distance, Theta, Wavelength: these must be set before resolution or
   measure calculations are sensible
#. Beam pixel Y, Z: these are the pixel coordinates of the main beam
   along fast & slow directions. Normally they will be picked up from
   the spdfil (which should be edited for your site), but they can be
   changed here. Click on the beam centre to get the `pixel
   coordinates <#output_pixel_coord>`__ in the output area, then click
   on menu item `Set Main Beam <#main_set_main_beam>`__, or
   alternatively type them in to these slots.
#. Outer circle: define resolution for the outer circle drawn by the
   `Circles <#main_circles>`__ option. Default to edge of detector. The
   `actual resolutions of the circles <#output_circle_resolution>`__ are
   given in the output area.

2) Output area
~~~~~~~~~~~~~~

#. Pixel: last pixel hit (Yms, Zms) or 1st corner of rectangle
#. Pixel coordinates expressed in Mosflm scanner frame in mm (as in the
   BEAM command to Mosflm)
#. Resolution: of last pixel hit (in Angstrom)
#. Spacing: result of `Measure <#main_measure>`__ option
#. Average, RMS, number: average, RMS deviation & number of pixels in
   the last selected rectangle
#. Zoom factor: if `Zoom <#main_zoom>`__ option used
#. Circle resolution: resolution of circles drawn by
   `Circles <#main_circles>`__ option

(3) Main menu
~~~~~~~~~~~~~

#. Current image: display whatever is in the current image array. Also
   useful for redrawing image if `Scale High & low <#display_scale>`__
   are changed
#. Read file: give name of file to be read & displayed
#. Read image: give image number to be displayed. The file name will be
   constructed using the template
#. Read next image: increment the image number by the image increment
   (normally = +1), display next image in series
#. Read prev image: decrement image number, display previous image
#. Set template: set filename template, as in Madnes (*i.e.* filename
   with # characters which will be replaced by an image number). Must be
   done before `Read image <#main_read_image>`__, `Add
   images <#main_add_images>`__, or `Overlay
   images <#main_overlay_images>`__ options can be used.
#. Scale image: set high & low image values to display as black or
   white. You may need to change the scale limits to improve the
   appearance the display.
#. Add images: add together a series of images (actually average them).
   You may wish to subtract a dark image from the sum. The `Write
   image <#main_write_image>`__ option may be used to save the result.
#. Overlay images: for a series of images, choose the largest value at
   each pixel. The `Write image <#main_write_image>`__ option may be
   used to save the result.
#. Correct image: apply various corrections (not usually useful). Click
   off the sub-menu to cancel
#. Write image: write the current image to disk. Useful with
   `Add <#main_add_images>`__ or `Overlay <#main_overlay_images>`__. The
   written image can be read back by ipdisp but may not be acceptable to
   other programs like mosflm.
#. Set Main Beam: set main beam pixel coordinates to last picked point,
   replace values in parameter table.
#. Fit circle: click on a series of points defining a circle centred on
   the main beam (*e.g.* a powder ring), then click this menu option
   again to fit a circle to the points. Determines the centre and radius
   of the best-fitting circle, allowing for a scale factor between the
   pixel size in the 2 directions. If the result is accepted, the main
   beam position is set to the centre of the circle, and the pixel size
   in the Yms (==Yc) direction is changed. Useful for determining main
   beam position from powder rings (*e.g.* wax). *N.B.* this fit is done
   in pixel coordinates multiplied by the pixel size: it does not take
   into account any spatial distortion (*e.g.* Roff & Toff), nor of
   detector tilt (theta)
#. Pick: toggle display of figure field around picked point on image.
#. Measure: toggle measurement of reciprocal lattice spacing. Pick two
   spots on a reciprocal lattice row (left button), then type number of
   orders, & the `lattice spacing <#output_spacing>`__ is output in the
   Output area. Beware skew cells, this gives you 1/a\* etc, not a!
#. Circles: toggle display of resolution circles. The `resolution of the
   outer circle <#display_outer_circle>`__ is defined in the parameter
   table, the `resolutions of all the
   circles <#output_circle_resolution>`__ is given in the output area.
   Note that for the Fast, the display is distorted (pixels are
   displayed as if they were square), so the circles are not circular.
#. Zoom: zoom a part of the image. Select an area by dragging a
   rectangle on the image (left button), then click Zoom. A square area
   enclosing the selected rectangle will be displayed. Zooming may be
   repeated. To restore full image, click Zoom when no rectangle is
   selected.
#. Exit: exit program

4) Image display area
~~~~~~~~~~~~~~~~~~~~~

    This is 800 x 800 pixels. Large images are sampled as necessary
    (*e.g.* 1 in 2, 1 in 3, or 1 in 4) on reading in. Use of
    `Zoom <#main_zoom>`__ allows the display of all data pixels for a
    part of any larger image.

Description of Spatial Distortion Files (spdfils)
-------------------------------------------------

The spatial distortion files (spdfil for short) contain information
which characterises the image file which is to be viewed - in turn these
characteristics are determined by 1. detector, 2. collection
software/hardware, 3. site specific setup.

The spdfils are short text files which can be viewed e.g. using the unix
command 'more'. A typical spdfil will contain several lines of comments
and two lines of data. The data lines look like:

::

    # IMGTYP   NHEAD   LRECL NPIXEL NPXREC    IMGDRC   ENDED
    #.......#.......#.......#.......#.......#.......#.......
    RAX            1    1024     950     950    +y+x  litend
    #
    #  YPXMAX    ZPXMAX    YBEAM     ZBEAM    YPXSIZ     ZPXSIZ      ROFF    TOFF
    #.........#.........#.........#.........#.........#.........#.........#.........
        950.       950.    425.0     425.0     0.2034    0.210     0.0       0.0

Lines beginning with # are comments and are ignored by IPDISP when
reading the file. The following sections describe the meaning of the
different items in the data lines, and how to customise existing spdfils
or create new ones.

Descriptions of spdfil data items
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Line 1: imgtyp, nhead, lrecl, npixel, npxrec, imgdrc, ended**

These entries have the following fixed fortran format: (A8, 4I8, 2A8),
*i.e.* imgtyp (string, 8 character field), nhead/lrecl/npixel/npxrec
(integers, each with an 8 character field), imgdrc/ended (strings, each
with 8 character field).

IMGTYP
    Image type, eg 'MAR', EMBL', 'RAX', 'MLDS', 'GEL' etc. Only used for
    reference purposes for the user.
NHEAD
    number of header records. (1 for MAR, 0 otherwise?)
LRECL
    record length (may be longer than real size; mustn't be smaller than
    NPIXEL)
NPIXEL
    number of pixels to read from each record; this corresponds to the
    number of pixels along the 'fast' axis.
NPXREC
    number of records to read; this corresponds to the number of pixels
    along the 'slow' axis.
IMGDRC
    image direction; string of the form *e.g.* -x+y which corresponds to
    the mosflm convention for the order and direction of the axes stored
    in the image file.
ENDED
    endedness of machine that wrote the image file:

    #. 'litend' (little-ended, ie least significant bit written first -
       this is the default) (Vax- or PC-like)
    #. 'bigend' (big-ended, ie most significant bit written first) (SGI,
       ESV, Sun, etc)

    This may also contain the string sqt (eg 'bigsqt') indicating
    squashed data above 32767, or ovf (*e.g.* 'bigovf') indicating
    Mar-style overflow table.

**Line 2: YPXMAX, ZPXMAX, YBEAM, ZBEAM, YPXSIZ, ZPXSIZ, ROFF, TOFF**

These entries have the following fixed fortran format: (8F10.4) *i.e.*
each of the entries are reals with a 10 character field and 4 decimal
places.

YPXMAX
    maximum usable pixel along Yms (*i.e.* fast axis)
ZPXMAX
    maximum usable pixel along Zms (*i.e.* slow axis)
YBEAM
    Yms coordinate of main beam (*i.e.* along fast axis, guess at
    YPXMAX/2)
ZBEAM
    Zms coordinate of main beam (i.e. along slow axis, guess at
    ZPXMAX/2)
YPXSIZ
    pixel size in mm along Yms
ZPXSIZ
    pixel size in mm along Zms
ROFF
    radial offset correction
TOFF
    tangential offset correction

Of these, NPIXEL, NPXREC, YPXSIZ and ZPXSIZ are detector properties.
IMGDRC, ENDED are collection characteristics. YBEAM and ZBEAM are site
specific.

ROFF and TOFF are corrections to do with scanning errors (the detectors
are scanned in a spiral and the data are then converted to a rectangular
grid for storage in the image file - these are corrections for the case
when the spiral is off-centre ... they should in any case be small so
set them both to zero if you don't have any better estimates).

Creating or customising spdfils
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Customisation may be necessary to correct for the alignment of the main
beam (YBEAM, ZBEAM), or if the detector operates in different scan modes
or with different pixel resolutions (NPIXEL, NPXREC, YPXSIZ, ZPXSIZ).
New detectors may require that you write your own spdfil, with the
caveat that some recent detectors (*e.g.* Mar345) write the image file
in a *packed* format, which IPDISP cannot read without coding changes.
In these cases it is not sufficient to write a new spdfil.

*e.g.* ADSC Quantum-4 CCD has an active area of 2304x2304 pixels with
pixels being 81.6x81.6 microns. This gives IMGTYP = 'ADSC' (used only
for reference), NPIXEL and NPXREC = 2304 and YPXSIZ and ZPXSIX = 0.0816.

Then: NRECL should be at least as big as NPIXEL (should be okay if they
are equal). YPXMAX and ZPXMAX can be set equal to NPIXEL and NPXREC
respectively, as defaults (not sure what these do).

YBEAM and ZBEAM together define the position of the main beam, and
ideally this would be at the centre of the image, *i.e.* YBEAM =
(YPXMAX/2) and ZBEAM = (ZPXMAX/2). In fact this is unlikely to be
exactly true for a real experimental setup, and these numbers will have
to be adjusted slightly. This can be done either by editing the spdfil
or from within IPDISP. Similarly, ROFF and TOFF are corrections for the
deviation for ideality of the radial scan, and are setup-specific.
Default to 0.0.

IPDISP will look for the spdfils in the directory $CCP4/x-windows/ipdisp
If you wish to have them elsewhere, you will have to edit the ipdisp
script.

List of distributed spdfils
---------------------------

This is the complete list of spdfils available at present; not all
spdfils have associated ipdisp options. Not all detectors are
represented and it will be necessary to create new spdfils for these
(see above). Otherwise MOSFLM will view images from most detectors,
*e.g.* Mar345.

+--------------------+--------------------+--------------------+--------------------+
| Unix               | Unix/VAX           | Spdfil name        | Detector           |
+====================+====================+====================+====================+
| -Y                 |                    |                    |                    |
| -BYlmb             |                    |                    |                    |
| spdfil.Mld         |                    |                    |                    |
| LMB Molecular      |                    |                    |                    |
| Dynamics scanner,  |                    |                    |                    |
| 1400x1400 pixel    |                    |                    |                    |
| scan               |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -Mo                |                    |                    |                    |
| -BOmar             |                    |                    |                    |
| spdfil.Sbigmar     |                    |                    |                    |
| MAR scanner (large |                    |                    |                    |
| plate, SGI,        |                    |                    |                    |
| original format    |                    |                    |                    |
| with overflow)     |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -z                 |                    |                    |                    |
| -ZLmb              |                    |                    |                    |
| spdfil.Smld        |                    |                    |                    |
| LMB Molecular      |                    |                    |                    |
| Dynamics scanner,  |                    |                    |                    |
| 512x456 pixel scan |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -ab                |                    |                    |                    |
| -AB                |                    |                    |                    |
| spdfil.adsc\_binne |                    |                    |                    |
| d                  |                    |                    |                    |
| 2x2 ADSC scanned   |                    |                    |                    |
| binned mode (\*)   |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -a                 |                    |                    |                    |
| -A                 |                    |                    |                    |
| spdfil.adsc\_unbin |                    |                    |                    |
| ned                |                    |                    |                    |
| 2x2 ADSC scanned   |                    |                    |                    |
| (\*)               |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -M                 |                    |                    |                    |
| -BMar              |                    |                    |                    |
| spdfil.bigmar      |                    |                    |                    |
| MAR scanner (large |                    |                    |                    |
| plate, SGI,        |                    |                    |                    |
| squashed data)     |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -V                 |                    |                    |                    |
| -BVaxmar           |                    |                    |                    |
| spdfil.bigvax      |                    |                    |                    |
| MAR scanner (large |                    |                    |                    |
| plate, Vax)        |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -o                 |                    |                    |                    |
| -OLmb              |                    |                    |                    |
| spdfil.film        |                    |                    |                    |
| Optronics film     |                    |                    |                    |
| scan (LMB scan     |                    |                    |                    |
| size)              |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -O                 |                    |                    |                    |
| -OOlmb             |                    |                    |                    |
| spdfil.film2560    |                    |                    |                    |
| Optronics film     |                    |                    |                    |
| scan (LMB scan     |                    |                    |                    |
| size, original     |                    |                    |                    |
| file)              |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -g                 |                    |                    |                    |
| -GEl               |                    |                    |                    |
| spdfil.gel         |                    |                    |                    |
| gel                |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -i2                |                    |                    |                    |
| -TIffgel           |                    |                    |                    |
| spdfil.lcl         |                    |                    |                    |
| TIFF file          |                    |                    |                    |
| translated from a  |                    |                    |                    |
| GEL file           |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -e                 |                    |                    |                    |
| -EMbl              |                    |                    |                    |
| spdfil.lmb         |                    |                    |                    |
| EMBL scanner       |                    |                    |                    |
| (prototype)        |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -m                 |                    |                    |                    |
| -MAr               |                    |                    |                    |
| spdfil.mar         |                    |                    |                    |
| MAR scanner (small |                    |                    |                    |
| plate, SGI)        |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -mc                |                    |                    |                    |
| -MCcd              |                    |                    |                    |
| spdfil.marccd      |                    |                    |                    |
| 130mm MAR CCD      |                    |                    |                    |
| scanner (\*)       |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -md                |                    |                    |                    |
| -MD                |                    |                    |                    |
| spdfil.marccd165   |                    |                    |                    |
| 165mm MAR CCD      |                    |                    |                    |
| scanner (\*)       |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -v                 |                    |                    |                    |
| -MVax              |                    |                    |                    |
| spdfil.marvax      |                    |                    |                    |
| MAR scanner (small |                    |                    |                    |
| plate, Vax)        |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -c                 |                    |                    |                    |
| -C                 |                    |                    |                    |
| spdfil.mcs         |                    |                    |                    |
| Mac Science        |                    |                    |                    |
| 2500x2500          |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -d3                |                    |                    |                    |
| -D3                |                    |                    |                    |
| spdfil.dip2030     |                    |                    |                    |
| Mac Science        |                    |                    |                    |
| DIP2030 (\*)       |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -d4                |                    |                    |                    |
| -D4                |                    |                    |                    |
| spdfil.dip2040     |                    |                    |                    |
| Mac Science        |                    |                    |                    |
| DIP2040 (\*)       |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -y                 |                    |                    |                    |
| -YLmb              |                    |                    |                    |
| spdfil.mld         |                    |                    |                    |
| LMB Molecular      |                    |                    |                    |
| Dynamics off-line  |                    |                    |                    |
| scanner, 700x700   |                    |                    |                    |
| pixel scan         |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -pf                |                    |                    |                    |
| -PF                |                    |                    |                    |
| spdfil.pf          |                    |                    |                    |
| Photon Factory     |                    |                    |                    |
| Weissenberg plate  |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -r                 |                    |                    |                    |
| -R                 |                    |                    |                    |
| spdfil.rax         |                    |                    |                    |
| RAXIS II scanner   |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -r4                |                    |                    |                    |
| -R4                |                    |                    |                    |
| spdfil.rax4100     |                    |                    |                    |
| RAXIS 4 scanner    |                    |                    |                    |
| with 100 micron    |                    |                    |                    |
| scan (\*)          |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -rc                |                    |                    |                    |
| -RC                |                    |                    |                    |
| spdfil.raxc        |                    |                    |                    |
| RAXIS II scanner   |                    |                    |                    |
| (coarse scan)      |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+
| -dc                |                    |                    |                    |
| -DC                |                    |                    |                    |
| spdfil.d2amccd     |                    |                    |                    |
| D2AM CCD scanner   |                    |                    |                    |
| (\*)               |                    |                    |                    |
+--------------------+--------------------+--------------------+--------------------+

Files for Mar345-style images, loaded automatically by ipdisp script:-

spdfil.mar1200, spdfil.mar1600, spdfil.mar1800, spdfil.mar2000,
spdfil.mar2300, spdfil.mar2400, spdfil.mar3000, spdfil.mar3450

*N.B.*: spdfils marked with (\*) are new and have not been widely
tested. Thanks to Dave Lawson for the RAXIS 4 spdfil, Sean McSweeney for
the Mar CCD and ADSC spdfils, Jean-Luc Ferrer for the D2AM CCD spdfil,
and to Atsushi Nakagawa for the MAC science DIP2030 and DIP2040 spdfils.

AUTHOR
------

Phil Evans, MRC LMB, Cambridge
