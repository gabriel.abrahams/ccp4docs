CIF2XML (CCP4: Supported Program)
=================================

NAME
----

**cif2xml** - Converting valid mmCIF files into XML files

| 

SYNOPSIS
--------

::

    cif2xml [mmCIF input file] [XML input file]

DESCRIPTION
-----------

CIF2XML is a program to convert an mmCIF Data Harvesting file into XML
format. mmCIF Data Harvesting files are typically obtained when running
certain CCP4 programs with the HARVEST keyword. The cif2xml program
takes the mmCIF data items and uses them to create XML tags, or
elements, with a 'ccif' prefix. Nested tags are used to represent mmCIF
loops.

EXAMPLES
--------

Runnable example
~~~~~~~~~~~~~~~~

`cif2xml.exam <../examples/unix/runnable/cif2xml.exam>`__

Example 1
~~~~~~~~~

Example of converting mmCIF files into XML files:

::

    cif2xml $CEXAM/data/red_aucn.scala $CCP4_SCR/red_aucn_scala.xml
