STGRID (CCP4: Supported Program)
================================

NAME
----

**stgrid** - Generate stereographic net plot for use with polarrfn

SYNOPSIS
--------

**stgrid plot** *file.plo*

DESCRIPTION
-----------

The PLOT84 plot file on logical name PLOT is a stereographic net
intended to be printed on a transparency for overlaying on the
stereographic projection plots produced by `polarrfn <polarrfn.html>`__.
The stereographic net produced by STGRID is used to measure the angular
co-ordinates on a stereographic projection. (To measure the angle
*between* a pair of points, see `STNET <stnet.html>`__.) Each point on
the projection corresponds to a point in polar angle space, which
represents an orientation of a rotated Patterson function.

The angle must be measured **along** any one of the great circles
(projected lines of longitude connecting opposite poles - note that the
sphere is projected so that the poles are at the left- and right-hand
edges and the equator runs vertically!). The other set of lines
(projected lines of latitude parallel to the equator) sub-divide the
great circles and are calibrated in degrees to allow the angle to be
read off.

Note that the net used must have exactly the same diameter as the plot,
otherwise the results will not be accurate.

The plot file may be converted to PostScript or HPGL for printing
(resetting line width) with `pltdev <pltdev.html>`__:

::

       pltdev -i file.plo -lnw .2

Print the PostScript file plot84.ps on your laser printer and photocopy
the stereographic net onto a transparent sheet.

BUGS
----

The diameter shown in the title (currently 17.1cm) assumes you are using
size A4 paper. If not, the diameter should still come out the same as
your plots from Polarrfn. Just edit the title in the call to PLTCTX and
recompile. However if you want to make a net with a specific diameter,
you'll have to unravel the mysteries of PLOT84 yourself - it probably
involves fiddling with the window parameters (see DATA statement for
array W in the source).

SEE ALSO
--------

`stnet <stnet.html>`__, `pltdev <pltdev.html>`__,
`plot84 <plot84.html>`__, `polarrfn <polarrfn.html>`__,
`rfcorr <rfcorr.html>`__

AUTHOR
------

Ian Tickle
