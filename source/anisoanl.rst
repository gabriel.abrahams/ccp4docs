ANISOANL (CCP4: Supported Program)
==================================

NAME
----

**anisoanl** - analyses of anisotropic displacement parameters

SYNOPSIS
--------

| **anisoanl xyzin** *input.pdb* **tlsin** *input.tls* **xyzout**
  *output.pdb* **tlsout** *output.tls* **psout** *out.ps*
| [Key-worded input file]

DESCRIPTION
-----------

The program analyses model anisotropic U values supplied on `ANISOU
records <pdbformat.html#part1ani>`__ in the input PDB file (XYZIN).
These may originate for example from a full anisotropic refinement by
`REFMAC <refmac5.html>`__ for atomic resolution data. Plots of average
equivalent isotropic B values, anisotropy, and radial and tangential
projections against residue are produced.

Using rigid groups defined in TLSIN, it can fit TLS values to the
observed U values. These are output in TLSOUT, and residual U values are
output in XYZOUT. The so-called L2 norm is used as the residual for
fitting U's calculated from TLS tensors to the observed Us obtained from
refinement.

The program can also analyse the input anisotropic U values in terms of
`Rosenfield's <#references>`__ rigid-body postulate. Output plots give
an indication of whether groups of atoms (as defined in TLSIN) have U
values conforming to rigid-body-like displacements. A postscript plot is
also produced which may hint at possible rigid groups.

The plots against residue may be useful for visualising U values
obtained from the program `TLSANL <tlsanl.html>`__. However, the rigid
group analysis is less useful, since in this case the U values will have
been obtained from a rigid group description in the first place.

INPUT AND OUTPUT FILES
----------------------

XYZIN
~~~~~

Input coordinates with anisotropic U values held in standard ANISOU
records. The elements of U are assumed to appear as integers
representing 10000\*Uij in orthogonal coordinates, and in the order U11,
U22, U23, U12, U13, U23.

The program will check for non-positive-definite anisotropic U values,
and report any found to the log file. Non-positive-definite means that
one of the eigenvalues is less than or equal to zero, which in turn
means that one of the radii of the thermal ellipsoid has vanished.

TLSIN
~~~~~

Rigid group definitions for TLS groups. The format of the TLS file is as
given in the `RESTRAIN <restrain.html>`__ documentation, with the
following addition. The RANGE record can include the subkeyword FIT or
APPLY. Atoms included in the FIT range are used both for fitting the TLS
values, and in calculating residual U's for XYZOUT. Atoms in APPLY are
used only for the latter. Thus, one may want to use only main chain
atoms for fitting TLS values, but then apply these TLS values to all
atoms in the molecule. Only TLS and RANGE records are required, to
define the chosen TLS groups.

XYZOUT
~~~~~~

Output coordinates and residual anisotropic U values.

TLSOUT
~~~~~~

Fitted TLS tensors for groups defined in TLSIN. This file can be input
to `TLSANL <tlsanl.html>`__ for further analysis. Note that TLSOUT
contains a REFMAC record to flag that the file contains tensor elements
in the order used in REFMAC, rather than the order used by RESTRAIN.

PSOUT
~~~~~

Postscript plot of "delta" values between all selected pairs of atoms.
Light shading implies low "delta" value, consistent with the atoms
belonging to the same quasi-rigid group. Dark shading means the atoms
are unlikely to belong to the same quasi-rigid group. See
`RIGIDBODY <#key_rigidbody>`__ keyword.

KEYWORDED INPUT
---------------

TITLE <title>
~~~~~~~~~~~~~

Title for the job.

FITTLS [OFF]
~~~~~~~~~~~~

Fit TLS parameters to individual anisotropic U values (default). If OFF
specified then don't. If TLS parameters are to be fitted, then the input
file TLSIN should be given.

TLSCYCLES <tlsc>
~~~~~~~~~~~~~~~~

Number of cycles of TLS fitting. With current residual, should converge
in first cycle! Default is 2 cycles.

RIGIDBODY [OFF]
~~~~~~~~~~~~~~~

Assess `Rosenfield's <#references>`__ rigid-body postulate (default). If
OFF specified then don't. The rigid-body postulate states that for two
atoms belonging to the same rigid-body (not necessarily bonded), the
projections of the anisotropic displacement parameters along the
interatomic vector should be equal. In practice, we expect the
difference between the projections to be smaller for atoms belonging to
the same quasi-rigid body, and larger for atoms belonging to different
quasi-rigid bodies.

For all pairs of atoms, the absolute difference in the projection as a
fraction of the mean projection (the "delta" value) is calculated, and
all such differences are binned (see the keyword
`DUBINS <#key_dubins>`__). These "delta" values are displayed
graphically in a postscript plot (file PSOUT, default anisoanl.ps).
Light shading implies low "delta" value, consistent with the atoms
belonging to the same quasi-rigid group. Dark shading means the atoms
are unlikely to belong to the same quasi-rigid group. Atom selection can
be done with the file TLSIN - only atoms specified in this file are used
in the calculation. For example, clearer results may be obtained if only
CA atoms are used. See Tom Schneider's `article <#references>`__ for an
example of this kind of plot.

The distribution of "delta" values is included in the log output (see
keywords `DUBINS <#key_dubins>`__ and `DURANGE <#key_durange>`__).
Possible quasi-rigid bodies should be defined using the TLSIN file (see
`example <#example2>`__ below). The distribution is plotted for all
pairs of atoms within each quasi-rigid body, and a final plot gives the
distribution for pairs of atoms from different groups. If the choice of
rigid bodies is good, the differences should be significantly smaller
within groups than between them. A subset of atoms can be chosen using
the atom selection field in TLSIN (e.g. "CA" may be useful for large
rigid groups).

DUBINS <dist\_bins> <ps\_bins>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Specify the number of bins for the `RIGIDBODY <#key_rigidbody>`__ plots.
<dist\_bins> refers to the distribution plot, and <ps\_bins> to the
postscript plot. Defaults are 30 and 10 (maximum is 100).

DURANGE <range>
~~~~~~~~~~~~~~~

Specify the range of values covered by the
`RIGIDBODY <#key_rigidbody>`__ plot. Default is 0.3. The "delta" values
are defined as the absolute difference in the projection as a fraction
of the mean projection, so the maximum range is 2.0

PLOT [OFF]
~~~~~~~~~~

Produce plots of equivalent isotropic U values, anisotropy, and
projections of U (default). If OFF specified then don't. See below for a
description of the plots produced.

MAINCHAIN
~~~~~~~~~

Use main chain atoms only in producing plots. Default is to use all
atoms.

VERBOSE
~~~~~~~

Produces extra output.

END
~~~

Terminate input.

PROGRAM OUTPUT
--------------

FITTLS option
~~~~~~~~~~~~~

For each cycle of fitting, the residual, the R value and the Goodness of
Fit are printed. The R value is sqrt(sum deltaU\*\*2 / sum Uobs\*\*2),
and the Goodness of Fit is 1000 \* sqrt(sum deltaU\*\*2 / (num
observables - num parameters)) (the factor of 1000 is one over a typical
sigmaU). These values are given for (i) atoms used in fitting (FIT),
(ii) atoms included in TLS group but not used for fitting (APPLY).

PLOT option
~~~~~~~~~~~

The PLOT option displays graphs of the following quantities:

Uiso
    The equivalent isotropic U factor calculated as 1/3\*trace(U).
R2FROMORIG
    The square of the distance from the local origin. If the FITTLS
    option is being used, then the local origin is taken to be the
    origin of the TLS group to which the atom belongs. Otherwise, the
    local origin is taken to be the centroid of the chain to which the
    atom belongs (i.e. the mean atomic coordinates of that chain). The
    values of R2FROMORIG are divided by a scaling factor (currently
    3000) for convenience of plotting.
Anisotropy
    This is defined as the ratio of the smallest to the largest
    eigenvalue of U.
PROLMEAN
    This factor is defined as the ratio of the middle to the largest
    eigenvalue of U. If the thermal ellipsoid corresponding to U is
    oblate (disc-like), then this factor will be close to 1. If however
    it is prolate (cigar-like), then this factor will be close to the
    value of the anisotropy
URADMEAN
    The projection of U onto a radial vector from the local origin (see
    above) to the atomic position.
UTANGMEAN
    The average value of U projected on to a plane perpendicular to the
    radial vector.
UISOMEAN2, ANISOMEAN2, PROLMEAN2
    If TLS groups have been fitted, then the values of Uiso, A and
    PROLMEAN as derived from the fitted TLS parameters are also given.
Radial distribution of Urad and Utang
    In a separate graph, Urad and Utang are plotted against R\*\*2. For
    rigid body motion, Urad should be constant, and Utang linear in
    R\*\*2.

All quantities are averaged over the atoms in each residue, unless the
`MAINCHAIN <#key_mainchain>`__ is given, in which case the average is
over main chain atoms only.

EXAMPLES
--------

Runnable example
~~~~~~~~~~~~~~~~

`anisoanl.exam <../examples/unix/runnable/anisoanl.exam>`__

Example 1
~~~~~~~~~

Example of fitting TLS groups, and plotting anisotropy etc.

::


    anisoanl xyzin holo_adh.pdb tlsin holo_adh.tls \
      xyzout holo_resid.pdb tlsout holo_out.tls <<eof
    FITTLS
    RIGIDBODY OFF
    PLOT
    MAINCHAIN
    END
    eof

Example 2
~~~~~~~~~

Example of the RIGIDBODY analysis:

::


    anisoanl xyzin 1exr.pdb tlsin 1exr.tls <<EOF
    FITTLS OFF
    RIGIDBODY
    DUBINS 8 10
    DURANGE 0.2
    PLOT OFF
    END
    EOF

where the rigid groups are defined in 1exr.tls as:

::


    REFMAC

    TLS    Chain A                                                                      
    RANGE  'A  16.' 'A  16.' CG CD1 CD2 CE1 CE2 CZ                       

    TLS    Chain A                                                                      
    RANGE  'A  19.' 'A  19.' CG CD1 CD2 CE1 CE2 CZ                         

The two rigid groups are two PHE side chains. This example gives a clear
indication of the two phenyl groups acting as rigid-bodies. Similar
results can be obtained for domain-size quasi-rigid bodies, though never
as clear-cut.

In this case, the postscript plot is unhelpful - it is more helpful for
looking at larger groups of atoms.

Published applications of ANISOANL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

arginine kinase transition-state analogue complex at 1.2A
    Yousef M.S., Fabiola F., Gattis J.L., Somasundarama T. and Chapman
    M.S. (2002) *Acta.Cryst.*, **D58**, 2009
calmodulin
    Wilson, M.A. and Brunger, A.T. (2003) *Acta.Cryst.*, **D59**, 1782
GroEL
    C Chaudhry, A L Horwich, A T Brunger and P D Adams (2004) *J. Mol.
    Biol.*, **342**, 229

SEE ALSO
--------

`tlsanl <tlsanl.html>`__ - analysis of TLS parameters

`RASTEP <http://www.bmsc.washington.edu/raster3d/>`__ (Raster3D Thermal
Ellipsoid Program) - plotting of thermal ellipsoids.

REFERENCES
----------

#. Martyn Winn, *CCP4 Newsletter* March 2001, **39**
   `ANISOANL - analysing anisotropic displacement
   parameters <http://www.ccp4.ac.uk/newsletter39/03_anisoanl.html>`__
#. R.E.Rosenfield, K.N.Trueblood and J.D.Dunitz, *Acta Cryst*, **A34**,
   828 - 829 (1978)
   Rigid-body postulate.
#. T.R.Schneider, *Proc. CCP4 Study Weekend*, 133 - 144 (1996).
   Application of rigid-body postulate to protein SP445.
#. V. Schomaker and K.N.Trueblood, *Acta Cryst.*, **B24**, 63 - 76
   (1968)
   Original description of TLS.
#. V. Schomaker and K.N.Trueblood, *Acta Cryst.*, **B54**, 507 - 514
   (1998)
   Description of THMA program for small molecules, which fits TLS
   parameters (and more) to refined U values.

AUTHORS
-------

Martyn Winn (m.d.winn@ccp4.ac.uk)
