Reindexing (CCP4: General)
==========================

NAME
----

reindexing - information about changing indexing regime

Contents
~~~~~~~~

-  `General Remarks <#general_remarks>`__
-  `A real monoclinic example <#monoclinic_eg>`__
-  `Lookup tables <#lookup_tables>`__
-  `Changing hand <#changing_hand>`__
-  `Pictures <#hklview_examples>`__

General Remarks
~~~~~~~~~~~~~~~

It is quite common to find that the diffraction from subsequent crystals
for a protein do not apparently merge well. There are many physical
reasons for this, but before throwing the data away it is sensible to
consider whether another indexing regime could be used. For
illustrations and examples see `HKLVIEW-examples
below <#hklview_examples>`__. For documentation on re-indexing itself,
and some hints, see also `REINDEX <reindex.html>`__.

For **orthorhombic** crystal forms with different cell dimensions along
each axis you can usually recognise if the next crystal is the same as
the last and see how to transform it (remember to keep your axial system
right-handed!).

| In **P1** and **P21** there are many ways of choosing axes, but they
  should all generate the same crystal volume. Use
  `MATTHEWS\_COEF <matthews_coef.html>`__ or some other method to check
  this - if the volumes *are not* the same, or at least related by
  integral factors, you have a new form. If they *are* the same it is
  recommended to plot some sections of the reciprocal lattice; you can
  often see that the patterns will match if you rotate in some way (see
  `HKLVIEW-examples below <#hklview_examples>`__). A common change in
  **P21** or **C2** where the twofold axis will be constant, is that
  a\*\ *new* = a\*\ *old* + c\*\ *old*, and c\*\ *new* must be chosen
  carefully. One very confusing case can arise if the length of
  (a\*+nc\*) is almost equal to that of a\* or nc\*, but it should be
  possible to sort out from the diffraction pattern plots.

Confusion arises mostly when two or more axes are the same length, as in
the **tetragonal**, **trigonal**, **hexagonal** or **cubic** systems. In
these cases any of the following definitions of axes is equally valid
and likely to be chosen by an auto-indexing procedure. The classic
description of this is that these are crystals where the Laue symmetry
is of a lower order than the apparent crystal lattice symmetry.

+--------------------+-----------------+------+-------------------+------+------------------+------+--------------------+
| real axes:         | (a,b,c)         | or   | (-a,-b,c)         | or   | (b,a,-c)         | or   | (-b,-a,-c)         |
+--------------------+-----------------+------+-------------------+------+------------------+------+--------------------+
| reciprocal axes:   | (a\*,b\*,c\*)   | or   | (-a\*,-b\*,c\*)   | or   | (b\*,a\*,-c\*)   | or   | (-b\*,-a\*,-c\*)   |
+--------------------+-----------------+------+-------------------+------+------------------+------+--------------------+

*N.B.* There are alternatives where other pairs of symmetry operators
are used, but this is the simplest and most general set of operators.
For example: in P4i (-a,-b,c) is equivalent to (-b,a,c), or in P3i
(-a,-b,c) is equivalent to (-b,a+b,c).

*N.B.* In general you should not change the hand of your axial system;
*i.e.* the determinant of the transformation matrix should be positive,
and only such transformations are discussed here.

| *The crystal symmetry may mean that some of these systems are already
  equivalent:*
| *For instance, if* (h,k,l) *is equivalent to* (-h,-k,l)*, the axial
  system pairs* [(a,b,c) and (-a,-b,c)] *and
  *\ [(b,a,-c) and (-b,-a,-c)] *are indistinguishable. This is the case
  for all tetragonal, hexagonal and cubic spacegroups.*
| *If* (h,k,l) *is equivalent to* (k,h,-l)*, the axial system pairs*
  [(a,b,c) and (b,a,-c)] *and
  *\ [(-a,-b,c) and (-b,-a,-c)] *are indistinguishable. This is true
  for* P4i2i2\ *,* P3i2\ *,* P6i22 *and some cubic spacegroups.*
| *If* (h,k,l) *is equivalent to* (-k,-h,-l)*, the axial system pairs*
  [(a,b,c) and (-b,-a,-c)] *and
  *\ [(-a,-b,c) and (b,a,-c)] *are indistinguishable. This is only true
  for* P3i12 *spacegroups.*
| *See detailed descriptions below*.

A real monoclinic example
~~~~~~~~~~~~~~~~~~~~~~~~~

Two datasets from the same crystal with apparently the same cell
dimensions:

run1 55.76 84.62 70.96 90 112.71 90

run2 56.04 85.11 71.57 90 113.15 90

However, the Rmerge from Scala was around 40% Viewing the k = constant
sections in `HKLVIEW <hklview.html>`__ showed that the diffraction
patterns were rotated with respect to each other. A reindexing
transformation of ``-h, -k, h+l`` was inferred. In terms of the
real-space cell, the new axes are constructed as follows:

|cell axes transformation|

Some basic trigonometry shows the new cell dimensions to be:

run1 (reindexed) 55.76 84.62 71.33 90 113.42 90

The new dimensions are slightly closer to those of run2. The previous
similarity is shown to be a coincidence.

Note the transformation ``-h, -k, h+l`` preserves the hand, i.e. the
corresponding matrix has a determinant of +1.

Lookup tables
~~~~~~~~~~~~~

Here are details for the possible systems:

-  All **P4i** and related **4i** space groups:
   (h,k,l) equivalent to (-h,-k,l) so we only need to check:
   +--------------------+-----------------+-------+------------------+
   | real axes:         | (a,b,c)         | and   | (b,a,-c)         |
   +--------------------+-----------------+-------+------------------+
   | reciprocal axes:   | (a\*,b\*,c\*)   | and   | (b\*,a\*,-c\*)   |
   +--------------------+-----------------+-------+------------------+

   *i.e.* check if reindexing (h,k,l) to (k,h,-l) gives a better match
   to previous data sets.
-  For all **P4i2i2** and related **4i2i2** space groups:
   (h,k,l) is equivalent to (-h,-k,l) *and* (k,h,-l) *and* (-k,-h,-l) so
   any choice of axial system will give identical data.
-  All **P3i** and **R3**:
   (h,k,l) *not* equivalent to (-h,-k,l) *or* (k,h,-l) or (-k,-h,-l) so
   we need to check all 4 possibilities:
   +--------------------+-----------------+-------+-------------------+-------+------------------+-------+--------------------+
   | real axes:         | (a,b,c)         | and   | (-a,-b,c)         | and   | (b,a,-c)         | and   | (-b,-a,-c)         |
   +--------------------+-----------------+-------+-------------------+-------+------------------+-------+--------------------+
   | reciprocal axes:   | (a\*,b\*,c\*)   | and   | (-a\*,-b\*,c\*)   | and   | (b\*,a\*,-c\*)   | and   | (-b\*,-a\*,-c\*)   |
   +--------------------+-----------------+-------+-------------------+-------+------------------+-------+--------------------+

   *i.e.* reindex (h,k,l) to (-h,-k,l) *or* (h,k,l) to (k,h,-l) *or*
   (h,k,l) to (-k,-h,-l).
   *N.B. For trigonal space groups, symmetry equivalent reflections can
   be conveniently described as* (h,k,l)*,* (k,i,l) *and* (i,h,l)
   *where* i=-(h+k). Replacing the 4 basic sets with a symmetry
   equivalent gives a bewildering range of possibilities!\ *.*
-  All **P3i12**:
   (h,k,l) already equivalent to (-k,-h,-l) so we only need to check:
   real axes:
   (a,b,c)
   and
   (b,a,-c)
   reciprocal axes:
   (a\*,b\*,c\*)
   and
   (b\*,a\*,-c\*)
   *i.e.* reindex (h,k,l) to (k,h,-l) which is equivalent here to
   reindexing (h,k,l) to (-h,-k,l).
-  All **P3i21** and **R32**:
   (h,k,l) already equivalent to (k,h,-l) so we only need to check:
   +--------------------+-----------------+-------+-------------------+
   | real axes:         | (a,b,c)         | and   | (-a,-b,c)         |
   +--------------------+-----------------+-------+-------------------+
   | reciprocal axes:   | (a\*,b\*,c\*)   | and   | (-a\*,-b\*,c\*)   |
   +--------------------+-----------------+-------+-------------------+

   *i.e.* reindex (h,k,l) to (-h,-k,l).
-  All **P6i**:
   (h,k,l) already equivalent to (-h,-k,l) so we only need to check:
   +--------------------+-----------------+-------+------------------+
   | real axes:         | (a,b,c)         | and   | (b,a,-c)         |
   +--------------------+-----------------+-------+------------------+
   | reciprocal axes:   | (a\*,b\*,c\*)   | and   | (b\*,a\*,-c\*)   |
   +--------------------+-----------------+-------+------------------+

   *i.e.* reindex (h,k,l) to (k,h,-l).
-  All **P6i2**:
   (h,k,l) already equivalent to (-h,-k,l) *and* (k,h,-l) *and*
   (-k,-h,-l) so we do not need to check.
-  All **P2i3** and related **2i3** space groups:
   (h,k,l) already equivalent to (-h,-k,l) so we only need to check:
   +--------------------+-----------------+-------+------------------+
   | real axes:         | (a,b,c)         | and   | (b,a,-c)         |
   +--------------------+-----------------+-------+------------------+
   | reciprocal axes:   | (a\*,b\*,c\*)   | and   | (b\*,a\*,-c\*)   |
   +--------------------+-----------------+-------+------------------+

   *i.e.* reindex (h,k,l) to (k,h,-l).
-  All **P4i32** and related **4i32** space groups:
   (h,k,l) already equivalent to (-h,-k,l) *and* (k,h,-l) *and*
   (-k,-h,-l) so we do not need to check.

Changing hand
~~~~~~~~~~~~~

[Bernhardinelli(1985)]

| Test to see if the other hand is the correct one:
| Change x,y,z for (cx-x, cy-y, cz-z)
| Usually (cx,cy,cz) = (0,0,0).

Remember you need to change the twist on the screw-axis stairs for P3i,
P4i, or P6i!

P2\ :sub:`1` - to P2\ :sub:`1`; For the half step of 2\ :sub:`1` axis,
the symmetry stays the same.

| P3\ :sub:`1` - to P3\ :sub:`2`
| P3\ :sub:`2` - to P3\ :sub:`1`

| P4\ :sub:`1` to P4\ :sub:`3`
| (P4:sub:`2` - to P4\ :sub:`2`: Half c axis step)
| P4\ :sub:`3` -to P4\ :sub:`1`

| P6\ :sub:`1` to P6\ :sub:`5`
| P6\ :sub:`2` - to P6\ :sub:`4`
| (P6:sub:`3` - to P6\ :sub:`3`)
| etc.

In a few non-primitive spacegroups, you can change the hand and not
change the spacegroup by a cunning shift of origin:

I4\ :sub:`1`
    (x,y,z) to (-x,1/2-y,-z)
I4\ :sub:`1`\ 22
    (x,y,z) to (-x,1/2-y,1/4-z)
F4\ :sub:`1`\ 32
    (x,y,z) to (1/4-x,1/4-y,1/4-z)

Plus some centric ones:

Fdd2
    (x,y,z) to (1/4-x,1/4-y,-z)
I4\ :sub:`1`\ md
    (x,y,z) to (1/4-x,1/4-y,-z)
I4\ :sub:`1`\ cd
    (x,y,z) to (1/4-x,1/4-y,-z)
I4bar2d
    (x,y,z) to (1/4-x,1/4-y,-z)

PICTURES
--------

Full size versions of the example pictures can be viewed by clicking on
the iconised ones.

+---------------------------------------+-----------------------------------------------------+
| |HKLVIEW Barnase pH6|                 | A P3\ :sub:`2` data set indexed h,k,l               |
+---------------------------------------+-----------------------------------------------------+
| |HKLVIEW Barnase pH6 indexed -h-kl|   | The same P3\ :sub:`2` data set, reindexed -h,-k,l   |
+---------------------------------------+-----------------------------------------------------+
| |HKLVIEW Barnase pH6 indexed -k-hl|   | The same P3\ :sub:`2` data set, reindexed -k,-h,l   |
+---------------------------------------+-----------------------------------------------------+
| |HKLVIEW Barnase pH6 indexed kh-l|    | The same P3\ :sub:`2` data set, reindexed k,h,-l    |
+---------------------------------------+-----------------------------------------------------+
|  |HKLVIEW Hipip h2l|                  | Monoclinic data set, HKLVIEW h,2,l                  |
+---------------------------------------+-----------------------------------------------------+
| |HKLVIEW Hipip reindexed|             | The same monoclinic data set, reindexed -h,-k,h-l   |
+---------------------------------------+-----------------------------------------------------+

AUTHORS
-------

| Eleanor Dodson, University of York, England
| Prepared for CCP4 by Maria Turkenburg, University of York, England
| Some additional material from Martyn Winn, Daresbury Lab.

REFERENCES
----------

[Bernhardinelli(1985)]
    G. Bernardinelli and H. D. Flack (1985). Least-squares
    absolute-structure refinement. Practical experience and ancillary
    calculations. Acta Cryst. A41, 500-511.

.. |cell axes transformation| image:: images/monoclinic_eg.gif
   :height: 300px
.. |HKLVIEW Barnase pH6| image:: images/bar_wt_ph6.gif
   :height: 100px
.. |HKLVIEW Barnase pH6 indexed -h-kl| image:: images/bar_wt_ph6_-h-kl.gif
   :height: 100px
.. |HKLVIEW Barnase pH6 indexed -k-hl| image:: images/bar_wt_ph6_-k-hl.gif
   :height: 100px
.. |HKLVIEW Barnase pH6 indexed kh-l| image:: images/bar_wt_ph6_kh-l.gif
   :height: 100px
.. |HKLVIEW Hipip h2l| image:: images/hipip-h2l.gif
   :height: 100px
.. |HKLVIEW Hipip reindexed| image:: images/hipip-h2l-reind.gif
   :height: 100px
