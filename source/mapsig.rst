MAPSIG (CCP4: Supported Program)
================================

NAME
----

**mapsig** - print statistics on signal/noise for translation function
map

SYNOPSIS
--------

| **mapsig** **MAPIN** *input1.map* **MAPIN2** *input2.map* **MAPOUT**
  *output.map* **PEAK\_LIST** *peaks.asc*
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

Print statistics on signal/noise for translation function map. Also
various arithmetic operations on 2 maps (sum, product, ratio).

INPUT AND OUTPUT FILES
----------------------

Input
~~~~~

MAPIN
    First map.
MAPIN2
    Second map, used if TYPE keyword is given.

When two maps are supplied, the maps may be different sizes provided
that the smaller one is MAPIN. This will then be automatically extended
in memory using translational symmetry only to match MAPIN2.

Output
~~~~~~

MAPOUT
    Combined map from TYPE operation. Output if MAPOUT keyword
    specified.
PEAK\_LIST
    Contains a list of the peaks found (also written to the log file).

.. _keywords:

KEYWORDED INPUT
---------------

Possible keywords are:

    `END <#end>`__, `MAPOUT <#mapout>`__,
    `PEAKS <#peaks>`__, `POINTS <#points>`__,
    `SCALES <#scales>`__, `TYPE <#type>`__

All keywords may be abbreviated to the first 4 characters, and all are
optional.

.. _points:

POINTS <NPM>
~~~~~~~~~~~~

Maximum number of map points to store for peak search (default = 40
times number of peaks requested).

.. _peaks:

PEAKS <NHM>
~~~~~~~~~~~

Maximum number of peaks to store (default = 10).

If more peaks are required (max 200), you need to store more points (up
to 8000), but beware the sort is very crude and very slow. For this
reason the program is not suitable as a general purpose peak searcher;
also although it allows for the non-primitive lattice translations in
translation functions, it takes no account of space- group symmetry,
which would give wrong answers for peaks on the map boundaries.

.. _scales:

SCALES <SC1> <SC2>
~~~~~~~~~~~~~~~~~~

SC1 = Scale factor for first map (default = 1). SC2 = Scale factor for
second map (default = 1).

.. _type:

TYPE <type>
~~~~~~~~~~~

| Type of combination: SUM, PRODUCT or RATIO (map 1 / map 2). The
  sub-keywords may be abbreviated to 1 character. Note that the product
  is divided by 100, and the ratio multiplied by 100.
| If TYPE is omitted, only one map (MAPIN) is processed, otherwise two
  maps (MAPIN and MAPIN2) are required.

.. _mapout:

MAPOUT [<title>]
~~~~~~~~~~~~~~~~

Provided TYPE is specified, the combined map is written to MAPOUT.

.. _end:

END
~~~

Equivalent to end-of-file: terminates data input.

EXAMPLES
--------

Example unix script found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`tffc\_procedure <../examples/unix/runnable/tffc_procedure>`__

SEE ALSO
--------

`mapmask <mapmask.html>`__

AUTHOR
------

Ian Tickle
