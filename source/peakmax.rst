PEAKMAX (CCP4: Supported Program)
=================================

NAME
----

**peakmax** - search for peaks in the electron density map

SYNOPSIS
--------

| **peakmax MAPIN** *foo\_in.map* [**XYZOUT** *foo\_out.pdb*]
  [**XYZFRC** *foo\_frac.out*] [**PEAKS** *foo\_peak.out*]
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

Search map (in standard map format) and list coordinates of all peaks
(optionally all positive peaks only) with height above a given
threshold. The peaks are listed in grid, fractional and orthogonal
coordinates, in the order they are found in the map and sorted in
descending order of absolute value of peak height. The list of peaks is
checked for symmetry-related peaks, and these are assigned a unique site
number to indicate which peaks are related. A peak list can be written
to output files: in a format with fractional coordinates suitable for
input to MLPHARE and/or to a PDB file for further processing, *e.g.*
with WATPEAK or graphics program such as O.

.. _keywords:

KEYWORDED INPUT
---------------

The various data control lines are identified by keywords, those
available being:

    `ATNAME <#atname>`__, `BFACTOR <#bfactor>`__,
    `CHAIN <#chain>`__, `EXCLUDE <#exclude>`__,
    `NUMPEAKS <#numpeaks>`__,
    `ORTHOGONALIZATION <#orthogonalization>`__,
    `OUTPUT <#output>`__, `PATTERSON <#patterson>`__,
    `RESIDUE <#residue>`__, `THRESHOLD <#threshold>`__,
    `XYZLIMIT <#xyzlimit>`__, `END <#end>`__

THRESHOLD [RMS] <threshold> [NEGATIVES]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| [default THRESHOLD RMS 3]
| Threshold for peaks: all peaks with values above <threshold> will be
  selected.
| **Subkeywords:**

     RMS
        peaks will be chosen if they are more than <threshold> \* (rms
        density), instead of an absolute value.
     NEGATIVES
        negative peaks below -<threshold> will be selected as well as
        positive peaks.

OUTPUT [ PDB \| FRAC \| PEAKS \| NONE ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| [default OUTPUT PDB]
| By default, output PDB files contain in the occupancy column, the peak
  height, and in the Bfactor column, height/rms(density). If the
  `BFACTOR <#bfactor>`__ command is present, these columns will be set
  to occupancy & Bfactor. Note that one or more of the subkeywords -
  other than 'NONE' - can appear on the same line as the OUTPUT keyword;
  they are not mutually exclusive.
| **Subkeywords:**

     PDB (default)
        write sorted peaks to a PDB format coordinate file with logical
        name XYZOUT
     FRAC
        write sorted peaks with fractional coordinates to the file with
        logical name XYZFRC, in a format suitable for input to MLPHARE.
        *N.B.*: In OUTPUT FRAC, no file will be written if the
        `PATTERSON <#patterson>`__ keyword is also present.
     PEAKS
        write unsorted peaks file to logical name PEAKS
     NONE
        no output file

BFACTOR <Bfactor> [<Occupancy>]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| [default: set occupancy = peak height/rms(density), Bfactor = height]
| Define Bfactor [& occupancy] for output file.

.. _residue:

RESIDUE <Residue\_type>
~~~~~~~~~~~~~~~~~~~~~~~

| [default HOH]
| Set residue type for output file.

.. _atname:

ATNAME <Atom\_name>
~~~~~~~~~~~~~~~~~~~

| [default O]
| Set atom name for output file.

.. _chain:

CHAIN <Chain\_ID>
~~~~~~~~~~~~~~~~~

| [default X]
| Set chain identifier for output file

.. _numpeaks:

NUMPEAKS <nop>
~~~~~~~~~~~~~~

| (<= 8000) (Default: 800)
| Number of peaks to output in the list, sorted by height

.. _orthogonalization:

ORTHOGONALIZATION <Ncode>
~~~~~~~~~~~~~~~~~~~~~~~~~

| [Default: 1 (Standard PDB)]
| Set orthogonalisation convention

::

                  =1   A // XO, C* // ZO  (Standard PDB)
                  =2   B // XO, A* // ZO
                  =3   C // XO, B* // ZO
                  =4  HEX A+B // XO, C* // ZO
                  =5   A* // XO, C // ZO      (Rollett)

.. _patterson:

PATTERSON
~~~~~~~~~

If given then the patterson vector lengths will be given for the sorted
list. If there is possible NCS translation the output is flagged.

::

                                    ***** NCS translation likely *****  

     Origin peak:                                        337.21   0.000 0.000 0.000     0.00   0.00   0.00
     Possible NCS Peak, fractional & Orthogonal coords    79.76   0.209 0.000 0.490    19.45   0.00  34.09

.. _xyzlimit:

XYZLIMIT <NU1> <NU2> <NV1> <NV2> <NW1> <NW2>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| [Default: If this keyword is omitted, the whole map is searched]
| Limits for search in grid units on fast, medium, slow axes.

.. _exclude:

EXCLUDE [ EDGE \| NONE ]
~~~~~~~~~~~~~~~~~~~~~~~~

| [Default: EXCLUDE EDGE]
| Attempt to suppress output of symmetry-equivalent peaks at the map
  edges (EXCLUDE EDGE), otherwise output all peaks (EXCLUDE NONE). It
  should be noted that EXCLUDE EDGE doesn't always successfully suppress
  all the equivalent peaks.

.. _end:

END
~~~

End input.

.. _output:

INPUT AND OUTPUT FILES
----------------------

Input
~~~~~

MAPIN
    Map file to be searched for peaks, in CCP4 map format.

Output
~~~~~~

Which of the following output files (if any) are produced depends on the
values of the subkeywords given with the `OUTPUT <#output>`__ keyword.
In all cases the height/(rms density) is written for each peak (*i.e.*
not the "absolute" peak height)

XYZOUT
    Sorted list of peaks written in PDB format; produced when the OUTPUT
    PDB option is specified.
    See also the `NOTES <#notes>`__ below.
XYZFRC
    Sorted list of peaks with fractional coordinates in a format
    suitable for input to MLPHARE; produced with the OUTPUT FRAC option.
    No file will be written if the `PATTERSON <#patterson>`__ keyword
    has been specified.
PEAKS
    List of unsorted peaks produced when the OUTPUT PEAKS option is
    specified.

NOTES
-----

In the PDB file on stream XYZOUT, the peaks are put out as waters with a
chain identifier of X until residue 999 is reached, when the sequence
continues from 1 and chain id Y. The official PDB format allows the
residue number to contain up to four digits, so this is technically
unnecessary.

SEE ALSO
--------

The program `WATPEAK <watpeak.html>`__ can be used to associate the
peaks/holes with atomic coordinates from the output with OUTPUT PDB
(default options).

AUTHOR
------

::

    Contact:   Phil Evans (6-OCT-1989 12:33) 
      Email :  PRE@MRC-MOLECULAR-BIOLOGY.CAMBRIDGE.AC.UK
        (pre@mrc-lmb.cam.ac.uk)

    Other authors: Ian Clifton (30-Jul-1993) 
      Address:  Laboratory of Molecular Biophysics
      Oxford University
      Rex Richards Building
      South Parks Road
      Oxford  OX1 3QU
      UK
      Phone:  +44 865 275387
      Email (INTERNET):  ian@biop.ox.ac.uk
      Email (X.400):  C=GB;ADMD= ;PRMD=UK.AC;O=OX;OU=BIOP;S=Ian 

EXAMPLES
--------

Simple unix example scripts found in $CEXAM/unix/runnable/
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| `3fo2fcmap <../examples/unix/runnable/3fo2fcmap>`__ (calculating a 3Fo
  - 2Fc map)

| `fofcmap <../examples/unix/runnable/fofcmap>`__ (calculating an Fo-Fc
  map)

| `watpeak.exam <../examples/unix/runnable/watpeak.exam>`__ (A simple
  Procedure for finding water peaks)

| `waterpeaks <../examples/unix/runnable/waterpeaks>`__ (Procedure for
  finding water peaks.)
