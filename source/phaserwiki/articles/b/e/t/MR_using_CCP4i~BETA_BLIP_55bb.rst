.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: MR using CCP4i:BETA/BLIP
   :name: mr-using-ccp4ibetablip
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

Data for this tutorial are found
`here <http://www.phaser.cimr.cam.ac.uk/index.php/Tutorials>`__

::

    Reflection data: beta_blip_P3221.mtz
    Structure files: beta.pdb, blip.pdb
    Sequence file: beta.seq, blip.seq

This tutorial demonstrates a difficult molecular replacement problem.

β-Lactamase (BETA, 29kDa) is an enzyme produced by various bacteria, and
is of interest because it is responsible for penicillin resistance,
cleaving penicillin at the β-lactam ring. There are many small molecule
inhibitors of BETA in clinical use, but bacteria can become resistant to
these as well. Streptomyces clavuligerus produces beta-lactamase
inhibitory protein (BLIP, 17.5kDa), which has been investigated as an
alternative to small molecule inhibitors, as it appears more difficult
for bacteria to become resistant to this form of BETA inhibition. The
structures of BETA and BLIP were originally solved separately by
experimental phasing methods. The crystal structure of the complex
between BETA and BLIP has been a test case for molecular replacement
because of the difficulty encountered in the original structure
solution. BETA, which models 62% of the unit cell, is trivial to locate,
but BLIP is more difficult to find. The BLIP component was originally
found by testing a large number of potential orientations with a
translation function search, until one solution stood out from the
noise.

#. What do you think is the best order in which to search for BETA and
   BLIP? Under what circumstances could the lower molecular weight
   search model be the easiest to find by molecular replacement?
#. What is the space-group recorded on the mtz file? If you had not
   solved this structure, would you know that this was the space-group?
   If not, what other space-group(s) must you consider?

   -  Think about handedness (enantiomorphs)

#. Run Phaser for solving BETA/BLIP

   -  Bring up the GUI for Phaser
   -  All the yellow boxes need to be filled in.
   -  Search for BETA and BLIP in the one job.

#. Has Phaser solved the structure?

   -  Look at the Z-scores for the rotation and translation functions

#. What search order was used?

   -  If you wanted, you could force the other search order and see what
      difference this makes.

#. Which space group was the solution in?
#. Look though the job.sum file and identify the anisotropy correction,
   rotation function, translation function, packing, and refinement
   modes, for the two search molecules, and all the space groups. Draw a
   flow diagram of the search strategy.
#. Why doesn't Phaser perform the rotation function in the two
   enantiomorphic space groups?
#. Which reflections in the data are particularly important for deciding
   the translational symmetry of the space-groups to search? Under what
   data collection conditions might you not have recorded these
   important reflections? Are there any other space-groups that you
   might want to consider when solving BETA/BLIP?
#. How big is the anisotropic correction for the data? How does this
   compare to TOXD?
#. Run Phaser again with the anisotropy correction turned off. What
   effect does this have on the structure solution?

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks">

.. raw:: html

   <div id="mw-normal-catlinks">

`Category <../../../../articles/c/a/t/Special%7ECategories_101d.html>`__:
`Tutorial <../../../../articles/t/u/t/Category%7ETutorial_c3b5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
