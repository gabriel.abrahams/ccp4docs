`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_spg.h File Reference
==========================

Data structure for symmetry information. `More... <#_details>`__

`Go to the source code of this file. <ccp4__spg_8h-source.html>`__

| 

Compounds
---------

struct  

**ccp4\_spacegroup\_**

struct  

**ccp4\_symop\_**

| 

Typedefs
--------

 typedef ccp4\_symop\_ 

**ccp4\_symop**

 typedef ccp4\_spacegroup\_ 

**CCP4SPG**

--------------

Detailed Description
--------------------

Data structure for symmetry information.

A data structure for spacegroup information and related quantities. Some
items are loaded from syminfo.lib while others are calculated on the
fly.

 **Author:**
    Martyn Winn
