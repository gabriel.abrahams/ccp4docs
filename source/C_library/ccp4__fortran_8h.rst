`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_fortran.h File Reference
==============================

| ``#include "ccp4_types.h"``

`Go to the source code of this file. <ccp4__fortran_8h-source.html>`__

| 

Defines
-------

 #define 

**\_LVTOB**\ (l)   ((long) ((l) == 0 ? 0 : 1))

 #define 

**\_BTOLV**\ (l)   ((int) ((l) == 0 ? 0 : 1))

 #define 

**FTN\_STR**\ (s)   s

 #define 

**FTN\_LEN**\ (s)   s##\_len

#define 

**char\_struct**\ (s)

#define 

**fill\_char\_struct**\ (s, str)

#define 

**init\_char\_struct**\ (s, str, size)

#define 

`FORTRAN\_SUBR <ccp4__fortran_8h.html#a7>`__\ (NAME, name, p\_sun,
p\_stardent, p\_mvs)   void name##\_ p\_sun

#define 

`FORTRAN\_CALL <ccp4__fortran_8h.html#a8>`__\ (NAME, name, p\_sun,
p\_stardent, p\_mvs)   name##\_ p\_sun

#define 

`FORTRAN\_FUN <ccp4__fortran_8h.html#a9>`__\ (val, NAME, name, p\_sun,
p\_stardent, p\_mvs)   val name##\_ p\_sun

 #define 

**FORTRAN\_LOGICAL\_TRUE**   1

 #define 

**FORTRAN\_LOGICAL\_FALSE**   0

| 

Typedefs
--------

 typedef pstr 

**fpstr**

 typedef unsigned int 

**ftn\_logical**

| 

Functions
---------

char \* 

`ccp4\_FtoCString <ccp4__fortran_8h.html#a15>`__ (fpstr str1, int
str1\_len)

void 

`ccp4\_CtoFString <ccp4__fortran_8h.html#a16>`__ (fpstr str1, int
str1\_len, const char \*cstring)

--------------

Detailed Description
--------------------

header file for Fortran APIs Eugene Krissinel

--------------

Define Documentation
--------------------

+--------------------------------------------------------------------------+
| +------------------------+------+------+-----+------+----+               |
| | #define char\_struct   | (    | s    |     | )    |    |               |
| +------------------------+------+------+-----+------+----+               |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | **Value:**                           |
|                                      |                                      |
|                                      | .. raw:: html                        |
|                                      |                                      |
|                                      |    <div class="fragment">            |
|                                      |                                      |
|                                      | ::                                   |
|                                      |                                      |
|                                      |     pstr  s;              \          |
|                                      |         int   s##_len;               |
|                                      |                                      |
|                                      | .. raw:: html                        |
|                                      |                                      |
|                                      |    </div>                            |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------+------+--------+-----+------+----+       |
| | #define fill\_char\_struct   | (    | s,     |                         |
| +------------------------------+------+--------+-----+------+----+       |
| |                              |      | str    |     | )    |    |       |
| +------------------------------+------+--------+-----+------+----+       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | **Value:**                           |
|                                      |                                      |
|                                      | .. raw:: html                        |
|                                      |                                      |
|                                      |    <div class="fragment">            |
|                                      |                                      |
|                                      | ::                                   |
|                                      |                                      |
|                                      |     s  = str;                      \ |
|                                      |         s##_len = strlen(str);       |
|                                      |                                      |
|                                      | .. raw:: html                        |
|                                      |                                      |
|                                      |    </div>                            |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+----------------+-----+------+-------- |
| --------------+                                                          |
| | #define FORTRAN\_CALL   | (    | NAME,          |                      |
| +-------------------------+------+----------------+-----+------+-------- |
| --------------+                                                          |
| |                         |      | name,          |                      |
| +-------------------------+------+----------------+-----+------+-------- |
| --------------+                                                          |
| |                         |      | p\_sun,        |                      |
| +-------------------------+------+----------------+-----+------+-------- |
| --------------+                                                          |
| |                         |      | p\_stardent,   |                      |
| +-------------------------+------+----------------+-----+------+-------- |
| --------------+                                                          |
| |                         |      | p\_mvs         |     | )    |    name |
| ##\_ p\_sun   |                                                          |
| +-------------------------+------+----------------+-----+------+-------- |
| --------------+                                                          |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Macro to call a Fortran subroutine   |
|                                      | from a C function.                   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *NAME*           | Subroutine  |
|                                      | name in upper case     |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *name*           | Subroutine  |
|                                      | name in lower case     |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *p\_sun*         | Argument li |
|                                      | st in Sun style        |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *p\_stardent*    | Argument li |
|                                      | st in Stardent style   |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *p\_mvs*         | Argument li |
|                                      | st in MVS style        |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------+------+----------------+-----+------+--------- |
| -----------------+                                                       |
| | #define FORTRAN\_FUN   | (    | val,           |                       |
| +------------------------+------+----------------+-----+------+--------- |
| -----------------+                                                       |
| |                        |      | NAME,          |                       |
| +------------------------+------+----------------+-----+------+--------- |
| -----------------+                                                       |
| |                        |      | name,          |                       |
| +------------------------+------+----------------+-----+------+--------- |
| -----------------+                                                       |
| |                        |      | p\_sun,        |                       |
| +------------------------+------+----------------+-----+------+--------- |
| -----------------+                                                       |
| |                        |      | p\_stardent,   |                       |
| +------------------------+------+----------------+-----+------+--------- |
| -----------------+                                                       |
| |                        |      | p\_mvs         |     | )    |    val n |
| ame##\_ p\_sun   |                                                       |
| +------------------------+------+----------------+-----+------+--------- |
| -----------------+                                                       |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Macro to define a function such that |
|                                      | it is callable as a Fortran          |
|                                      | function.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *val*            | Data type o |
|                                      | f return value.        |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *NAME*           | Function na |
|                                      | me in upper case       |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *name*           | Function na |
|                                      | me in lower case       |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *p\_sun*         | Argument li |
|                                      | st in Sun style        |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *p\_stardent*    | Argument li |
|                                      | st in Stardent style   |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *p\_mvs*         | Argument li |
|                                      | st in MVS style        |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------+------+----------------+-----+------+-------- |
| -------------------+                                                     |
| | #define FORTRAN\_SUBR   | (    | NAME,          |                      |
| +-------------------------+------+----------------+-----+------+-------- |
| -------------------+                                                     |
| |                         |      | name,          |                      |
| +-------------------------+------+----------------+-----+------+-------- |
| -------------------+                                                     |
| |                         |      | p\_sun,        |                      |
| +-------------------------+------+----------------+-----+------+-------- |
| -------------------+                                                     |
| |                         |      | p\_stardent,   |                      |
| +-------------------------+------+----------------+-----+------+-------- |
| -------------------+                                                     |
| |                         |      | p\_mvs         |     | )    |    void |
|  name##\_ p\_sun   |                                                     |
| +-------------------------+------+----------------+-----+------+-------- |
| -------------------+                                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Macro to define a function such that |
|                                      | it is callable as a Fortran          |
|                                      | subroutine.                          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *NAME*           | Subroutine  |
|                                      | name in upper case     |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *name*           | Subroutine  |
|                                      | name in lower case     |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *p\_sun*         | Argument li |
|                                      | st in Sun style        |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *p\_stardent*    | Argument li |
|                                      | st in Stardent style   |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
|                                      |     | *p\_mvs*         | Argument li |
|                                      | st in MVS style        |             |
|                                      |     +------------------+------------ |
|                                      | -----------------------+             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +------------------------------+------+---------+-----+------+----+      |
| | #define init\_char\_struct   | (    | s,      |                        |
| +------------------------------+------+---------+-----+------+----+      |
| |                              |      | str,    |                        |
| +------------------------------+------+---------+-----+------+----+      |
| |                              |      | size    |     | )    |    |      |
| +------------------------------+------+---------+-----+------+----+      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | **Value:**                           |
|                                      |                                      |
|                                      | .. raw:: html                        |
|                                      |                                      |
|                                      |    <div class="fragment">            |
|                                      |                                      |
|                                      | ::                                   |
|                                      |                                      |
|                                      |     s  = str;                      \ |
|                                      |         s##_len = size;              |
|                                      |                                      |
|                                      | .. raw:: html                        |
|                                      |                                      |
|                                      |    </div>                            |
+--------------------------------------+--------------------------------------+

--------------

Function Documentation
----------------------

void ccp4\_CtoFString

( 

fpstr 

  *str1*,

int 

  *str1\_len*,

const char \* 

  *cstring*

) 

+--------------------------------------+--------------------------------------+
|                                      | Creates a Fortran string from an     |
|                                      | input C string for passing back to   |
|                                      | Fortran call. Characters after       |
|                                      | null-terminator may be junk, so pad  |
|                                      | with spaces. If input cstring is     |
|                                      | NULL, return blank string.           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
|                                      |     | *str1*         | pointer Fortr |
|                                      | an to string   |                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
|                                      |     | *str1\_len*    | Fortran lengt |
|                                      | h of string    |                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
|                                      |     | *cstring*      | input C strin |
|                                      | g              |                     |
|                                      |     +----------------+-------------- |
|                                      | ---------------+                     |
+--------------------------------------+--------------------------------------+

char\* ccp4\_FtoCString

( 

fpstr 

  *str1*,

int 

  *str1\_len*

) 

+--------------------------------------+--------------------------------------+
|                                      | Creates a null-terminated C string   |
|                                      | from an input string obtained from a |
|                                      | Fortran call. Trailing blanks are    |
|                                      | removed. If input string is blank    |
|                                      | then return string "\\0". Memory     |
|                                      | assigned by malloc, so can be freed. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +----------------+-------------- |
|                                      | --------------+                      |
|                                      |     | *str1*         | pointer to st |
|                                      | ring          |                      |
|                                      |     +----------------+-------------- |
|                                      | --------------+                      |
|                                      |     | *str1\_len*    | Fortran lengt |
|                                      | h of string   |                      |
|                                      |     +----------------+-------------- |
|                                      | --------------+                      |
+--------------------------------------+--------------------------------------+
