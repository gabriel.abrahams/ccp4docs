`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CINCH - Crystallography IN C Headers
====================================

 **Note:**
    I'm fed up of the uninspiring "new library" and here's my first
    attempt at a funky name. Alternatives welcome!

.. raw:: html

   <div class="fragment">

::




       

.. raw:: html

   </div>

Aims
----

The CCP4 software suite is based around a library of routines which
cover common tasks, such as file opening, parsing keyworded input,
reading and writing of standard data formats, applying symmetry
operations, etc. Programs in the suite call these routines which, as
well as saving the programmer some effort, ensure that the varied
programs in the suite have a similar look-and-feel.

Since 2002, there has been a major effort to re-write much of the CCP4
library into C/C++. The aims are:

-  To implement a better representation of the underlying data model.
   For example, Eugene Krissinel's MMDB library acts on a data structure
   which represents the various levels of structure of a protein model.
   The new `MTZ <structMTZ.html>`__ library encapsulates the
   crystal/dataset hierarchy that is increasingly being used by
   programs.
-  To maintain support for existing programs. In particular, the
   existing Fortran APIs will be maintained, although they will now
   often be only wrappers to functions in the new library. It is hoped
   that many existing programs will be migrated to using the new library
   directly.
-  To provide support for scripting. It is possible to generate APIs for
   Python, Tcl and Perl automatically from the core C code. Thus, much
   of the standard CCP4 functionality wil be available to scripts used
   e.g. in ccp4i or the molecular graphics project.

This incremental approach, maintaining the existing suite while
improving the underlying code, puts constraints on what is possible, but
is considered more appropriate for a collaborative project like CCP4.

This documentation
------------------

This documentation is generated automatically by
`Doxygen <http://www.doxygen.org/>`__ from comment sections in the code.
It is therefore detailed and extensive. The library divides roughly into
the following sections:

CMTZ library
    See the `CMTZ library <cmtz_page.html#cmtz_page>`__ page for C/C++
    programmers, and the `Fortran API to
    CMTZ <cmtz_f_page.html#cmtz_f_page>`__ page for Fortran programmers.
CMAP library
    See the `CMAP library <cmap_page.html#cmap_page>`__ page for C/C++
    programmers, and the `Fortran API to
    CMAP <cmap_f_page.html#cmap_f_page>`__ page for Fortran programmers.
MMDB library
    See Eugene's `documentation <http://www.ebi.ac.uk/~keb/cldoc>`__.
CSYM library
    See the `CSYM library <csym_page.html#csym_page>`__ page for C/C++
    programmers, and the `Fortran API to
    CSYM <csym_f_page.html#csym_f_page>`__ page for Fortran programmers.
CCP4 utility library
    See the `CCP4 Library
    Utilities <utilities_page.html#utilities_page>`__ page for C/C++
    programmers.
Low level disk i/o
    See the `Fortran API for low level
    input/output. <diskio_f_page.html#diskio_f_page>`__ page for Fortran
    programmers.
