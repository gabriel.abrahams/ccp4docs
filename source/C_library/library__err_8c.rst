`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

library\_err.c File Reference
=============================

| ``#include <stdlib.h>``
| ``#include <stdio.h>``
| ``#include <string.h>``
| ``#include "ccp4_errno.h"``

| 

Compounds
---------

struct  

**error\_system**

| 

Functions
---------

 const char \* 

**ccp4\_strerror** (int error)

 void 

**ccp4\_error** (const char \*msg)

 void 

**ccp4\_fatal** (const char \*message)

 int 

**CFile\_Perror** (const char \*msg)

 int 

**ccp4\_liberr\_verbosity** (int iverb)

 void 

**ccp4\_signal** (const int code, const char \*const msg,
void(\*callback)())

| 

Variables
---------

int 

`ccp4\_errno <library__err_8c.html#a1>`__ = 0

--------------

Detailed Description
--------------------

Error handling library. Charles Ballard

--------------

Variable Documentation
----------------------

+--------------------------------------------------------------------------+
| +-----------------------+                                                |
| | int ccp4\_errno = 0   |                                                |
| +-----------------------+                                                |
+--------------------------------------------------------------------------+

+-----+---------------------------------------------+
|     | @global ccp4\_errno: global to store data   |
+-----+---------------------------------------------+
