`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

MTZ Member List
===============

This is the complete list of members for `MTZ <structMTZ.html>`__,
including all inherited members.

+--------------------------------------------+----------------------------+----+
| `batch <structMTZ.html#m16>`__             | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `filein <structMTZ.html#m0>`__             | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `fileout <structMTZ.html#m1>`__            | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `hist <structMTZ.html#m3>`__               | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `histlines <structMTZ.html#m4>`__          | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `mnf <structMTZ.html#m13>`__               | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `mtzsymm <structMTZ.html#m14>`__           | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `n\_orig\_bat <structMTZ.html#m10>`__      | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `ncol\_read <structMTZ.html#m6>`__         | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `nref <structMTZ.html#m7>`__               | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `nref\_filein <structMTZ.html#m8>`__       | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `nxtal <structMTZ.html#m5>`__              | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `order <structMTZ.html#m17>`__             | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `refs\_in\_memory <structMTZ.html#m9>`__   | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `resmax\_out <structMTZ.html#m11>`__       | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `resmin\_out <structMTZ.html#m12>`__       | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `title <structMTZ.html#m2>`__              | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
| `xtal <structMTZ.html#m15>`__              | `MTZ <structMTZ.html>`__   |    |
+--------------------------------------------+----------------------------+----+
