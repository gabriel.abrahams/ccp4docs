`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmtzlib.h
=========

`Go to the documentation of this file. <cmtzlib_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      cmtzlib.h: header file for cmtzlib.c
    00003      Copyright (C) 2001  CCLRC, Martyn Winn
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00095 #ifndef __CMTZLib__
    00096 #define __CMTZLib__
    00097 
    00098 static char rcsidhm[100] = "$Id$";
    00099 
    00100 /* defines CCP4::CCP4File */
    00101 #include "ccp4_utils.h"
    00102 
    00103 #ifdef  __cplusplus
    00104 namespace CMtz {
    00105 extern "C" {
    00106 typedef CCP4::CCP4File CCP4File;
    00107 #endif
    00108 
    00109 /* needs to be here for C++ to use CCP4File typedef */
    00110 #include "mtzdata.h"
    00111 
    00112 /**** MTZ i/o ****/
    00113 
    00120 MTZ *MtzGet(const char *logname, int read_refs);
    00121 
    00133 MTZ *MtzGetUserCellTolerance(const char *logname, int read_refs, const double cell_tolerance);
    00134 
    00141 int MtzRrefl(CCP4File *filein, int ncol, float *refldata);
    00142 
    00149 int MtzPut(MTZ *mtz, const char *logname);
    00150 
    00158 CCP4File *MtzOpenForWrite(const char *logname);
    00159 
    00167 int MtzWhdrLine(CCP4File *fileout, int nitems, char buffer[]);
    00168 
    00176 int MtzWrefl(CCP4File *fileout, int ncol, float *refldata);
    00177 
    00184 int MtzDeleteRefl(MTZ *mtz, int iref);
    00185 
    00186 /**** Memory allocation ****/
    00187 
    00199 MTZ *MtzMalloc(int nxtal, int nset[]);
    00200 
    00205 int MtzFree(MTZ *mtz);
    00206 
    00213 MTZCOL *MtzMallocCol(MTZ *mtz, int nref);
    00214 
    00219 int MtzFreeCol(MTZCOL *col);
    00220 
    00224 MTZBAT *MtzMallocBatch(void);
    00225 
    00230 int MtzFreeBatch(MTZBAT *batch);
    00231   
    00236 char *MtzCallocHist(int nhist);
    00237 
    00242 int MtzFreeHist(char *hist);
    00243 
    00249 void MtzMemTidy(void);
    00250 
    00251 /**** Header operations ****/
    00252   
    00257 int MtzNbat(const MTZ *mtz);
    00258 
    00263 int MtzNref(const MTZ *mtz);
    00264 
    00269 int MtzSpacegroupNumber(const MTZ *mtz);
    00270 
    00278 int MtzResLimits(const MTZ *mtz, float *minres, float *maxres);
    00279 
    00280 /**** Crystal operations ****/
    00281 
    00286 int MtzNxtal(const MTZ *mtz);
    00287 
    00292 int MtzNumActiveXtal(const MTZ *mtz);
    00293 
    00298 MTZXTAL **MtzXtals(MTZ *mtz);
    00299 
    00305 MTZXTAL *MtzIxtal(const MTZ *mtz, const int ixtal);
    00306 
    00313 char *MtzXtalPath(const MTZXTAL *xtal);
    00314 
    00320 MTZXTAL *MtzXtalLookup(const MTZ *mtz, const char *label);
    00321 
    00329 MTZXTAL *MtzAddXtal(MTZ *mtz, const char *xname, const char *pname,
    00330                   const float cell[6]);
    00331 
    00336 int MtzNsetsInXtal(const MTZXTAL *xtal);
    00337 
    00342 int MtzNumActiveSetsInXtal(const MTZ *mtz, const MTZXTAL *xtal);
    00343 
    00349 MTZSET **MtzSetsInXtal(MTZXTAL *xtal);
    00350 
    00357 MTZSET *MtzIsetInXtal(const MTZXTAL *xtal, const int iset);
    00358 
    00359 /**** Dataset operations ****/
    00360 
    00365 int MtzNset(const MTZ *mtz);
    00366 
    00371 int MtzNumActiveSet(const MTZ *mtz);
    00372 
    00380 MTZXTAL *MtzSetXtal(const MTZ *mtz, const MTZSET *set);
    00381 
    00390 char *MtzSetPath(const MTZ *mtz, const MTZSET *set);
    00391 
    00398 MTZSET *MtzSetLookup(const MTZ *mtz, const char *label);
    00399 
    00407 MTZSET *MtzAddDataset(MTZ *mtz, MTZXTAL *xtl, const char *dname,
    00408                     const float wavelength);
    00409 
    00416 int MtzNcolsInSet(const MTZSET *set);
    00417 
    00422 int MtzNumActiveColsInSet(const MTZSET *set);
    00423 
    00429 int MtzNumSourceColsInSet(const MTZSET *set);
    00430 
    00436 int MtzNbatchesInSet(const MTZ *mtz, const MTZSET *set);
    00437 
    00443 MTZCOL **MtzColsInSet(MTZSET *set);
    00444 
    00452 MTZCOL *MtzIcolInSet(const MTZSET *set, const int icol);
    00453 
    00454 /**** Column operations ****/
    00455 
    00463 MTZCOL *MtzAddColumn(MTZ *mtz, MTZSET *set, const char *label,
    00464                    const char *type);
    00465 
    00470 int MtzAssignHKLtoBase(MTZ *mtz);
    00471 
    00484 int MtzAssignColumn(MTZ *mtz, MTZCOL *col, const char crystal_name[],  
    00485              const char dataset_name[]);
    00486 
    00493 int MtzToggleColumn(MTZCOL *col);
    00494 
    00501 MTZSET  *MtzColSet(const MTZ *mtz, const MTZCOL *col);
    00502 
    00507 int MtzNcol(const MTZ *mtz);
    00508   
    00513 int MtzNumActiveCol(const MTZ *mtz);
    00514   
    00520 int MtzNumSourceCol(const MTZ *mtz);
    00521 
    00529 char *MtzColPath(const MTZ *mtz, const MTZCOL *col);
    00530 
    00537 int MtzRJustPath(char *path, const char *partial, const int njust);
    00538 
    00544 int MtzPathMatch(const char *path1, const char *path2);
    00545 
    00551 MTZCOL *MtzColLookup(const MTZ *mtz, const char *label);
    00552 
    00557 char *MtzColType(MTZCOL *col);
    00558 
    00564 void MtzDebugHierarchy(const MTZ *mtz);
    00565 
    00573 int MtzListColumn(const MTZ *mtz, char clabs[][31], char ctyps[][3], int csetid[]);
    00574 
    00582 int MtzListInputColumn(const MTZ *mtz, char clabs[][31], char ctyps[][3], int csetid[]);
    00583 
    00584 /**** helper functions ****/
    00585 
    00594 int MtzFindInd(const MTZ *mtz, int *ind_xtal, int *ind_set, int ind_col[3]);
    00595 
    00602 float MtzInd2reso(const int in[3], const double coefhkl[6]);
    00603 
    00609 int MtzHklcoeffs(const float cell[6], double coefhkl[6]);
    00610 
    00617 int MtzArrayToBatch(const int *intbuf, const float *fltbuf, MTZBAT *batch);
    00618 
    00625 int MtzBatchToArray(MTZBAT *batch, int *intbuf, float *fltbuf);
    00626 
    00635 MTZBAT *sort_batches(MTZBAT *batch, int numbat);
    00636 
    00637 /**** pseudo-mtzlib routines ****/
    00638 
    00645 int ccp4_lrtitl(const MTZ *mtz, char *title);
    00646 
    00653 int ccp4_lrhist(const MTZ *mtz, char history[][MTZRECORDLENGTH], int nlines);
    00654 
    00660 int ccp4_lrsort(const MTZ *mtz, int isort[5]);
    00661 
    00668 int ccp4_lrbats(const MTZ *mtz, int *nbatx, int batchx[]);
    00669 
    00675 int ccp4_lrcell(const MTZXTAL *xtl, float cell[]);
    00676 
    00686 int ccp4_lrsymi(const MTZ *mtz, int *nsympx, char *ltypex, int *nspgrx, 
    00687        char *spgrnx, char *pgnamx);
    00688 
    00698 int ccp4_lrsymm(const MTZ *mtz, int *nsymx, float rsymx[192][4][4]);
    00699 
    00709 int MtzParseLabin(char *labin_line, const char prog_labels[][31], 
    00710            const int nlprgi, char user_labels[][2][31]);
    00711 
    00725 MTZCOL **ccp4_lrassn(const MTZ *mtz, const char labels[][31], const int nlabels, 
    00726                 char types[][3]);
    00727 
    00741 int ccp4_lridx(const MTZ *mtz, const MTZSET *set, char crystal_name[64], 
    00742             char dataset_name[64], char project_name[64], int *isets, 
    00743             float datcell[6], float *datwave);
    00744 
    00760 int ccp4_lrrefl(const MTZ *mtz, float *resol, float adata[], int logmss[], int iref);
    00761 
    00778 int ccp4_lrreff(const MTZ *mtz, float *resol, float adata[], int logmss[],
    00779                 const MTZCOL *lookup[], const int ncols, const int iref);
    00780 
    00788 int ccp4_ismnf(const MTZ *mtz, const float datum);
    00789 
    00795 int ccp4_lhprt(const MTZ *mtz, int iprint);
    00796 
    00803 int ccp4_lhprt_adv(const MTZ *mtz, int iprint);
    00804 
    00812 int ccp4_lrbat(MTZBAT *batch, float *buf, char *charbuf, int iprint);
    00813 
    00818 int MtzPrintBatchHeader(const MTZBAT *batch);
    00819 
    00827 int ccp4_lwtitl(MTZ *mtz, const char *ftitle, int flag);
    00828 
    00838 int MtzSetSortOrder(MTZ *mtz, MTZCOL *colsort[5]);
    00839 
    00846 int MtzAddHistory(MTZ *mtz, const char history[][MTZRECORDLENGTH], const int nlines);
    00847 
    00862 int ccp4_lwsymm(MTZ *mtz, int nsymx, int nsympx, float rsymx[192][4][4], 
    00863                 char ltypex[], int nspgrx, char spgrnx[], char pgnamx[]);
    00864 
    00865 /* Assign columns for writing. Check to see if columns already exist,
    00866  * else create them. New columns are assigned to the base dataset if it 
    00867  * exists, else the first dataset.
    00868  * @param mtz pointer to MTZ struct
    00869  * @param labels Input array of column labels to be assigned.
    00870  * @param nlabels Number of columns.
    00871  * @param types Input array of column types of columns.
    00872  * @param iappnd If iappnd = 0, then deactivate columns which are
    00873  *   not selected (allows one to write out a subset of columns). Else
    00874  *   if iappnd = 1, append these columns to existing ones.
    00875  * @return Array of pointers to columns in MTZ struct.
    00876  */
    00877 MTZCOL **ccp4_lwassn(MTZ *mtz, const char labels[][31], const int nlabels, 
    00878              const char types[][3], const int iappnd);
    00879 
    00880 /* Add or update a dataset in the MTZ structure. If the crystal name is
    00881  * not recognised, then a new crystal is created containing a single
    00882  * dataset. If the crystal name is recognised, then the parameters of
    00883  * the crystal are updated. The child dataset is then created if necessary
    00884  * or an existing dataset updated.
    00885  * Note that this function is used to update crystal parameters, as well
    00886  * as add crystals. If a duplicate crystal name is used by mistake, then 
    00887  * the old crystal parameters are lost.
    00888  * @param mtz pointer to MTZ struct
    00889  * @param crystal_name Crystal name
    00890  * @param dataset_name Dataset name
    00891  * @param project_name Project name
    00892  * @param datcell Cell dimensions of crystal that dataset belongs to.
    00893  * @param datwave X-ray wavelength associated with dataset.
    00894  * @return 1 on success, 0 on failure
    00895  */
    00896 int ccp4_lwidx(MTZ *mtz, const char crystal_name[],  const char dataset_name[],
    00897         const char project_name[], const float datcell[6], const float *datwave);
    00898 
    00899 
    00917 int ccp4_lwrefl(MTZ *mtz, const float adata[], MTZCOL *lookup[], 
    00918                  const int ncol, const int iref);
    00919 
    00933 int ccp4_lwbat(MTZ *mtz, MTZBAT *batch, const int batno, const float *buf, const char *charbuf);
    00934 
    00935 int ccp4_lwbsetid(MTZ *mtz, MTZBAT *batch, const char xname[], const char dname[]);
    00936 
    00937 /* -- Below here there are no implementations -- */
    00938 
    00939 /* COMPLEX HLToSF(float hla, float hlb, float hlc, float hld, BOOLEAN centric); */
    00940 /* Returns the mean structure factor as a complex number from a structure
    00941    factor probability distribution described by Hendrickson/Lattman
    00942    coefficients. If `centric == TRUE`, the coefficients describe a centric
    00943    distribution. */
    00944 
    00945 /* MTZ *MtzSort(MTZ *mtz, char *ident); */
    00946 /* Sorts `mtz` using the identifiers (separated by spaces) in `ident` as
    00947    keys. Sorting can be done on up to 200 columns. A pointer to `*mtz` is
    00948    returned. */
    00949 
    00950 /* MTZ *HLCombine (MTZ *to, float toscale, MTZ *frm, float frmscale); */
    00951 /* Combines extra phase information for common reflections between 'frm'
    00952    and 'to' into the phase information of 'to'. The phase probabilities
    00953    are described by Hendrickson Lattman coefficients, with the identifiers
    00954    "HLA", "HLB", HLC", and "HLD", the indices are identified by "H", "K" 
    00955    and "L". HL-coeffs from 'to' are scaled by 'toscale', the coeffs from
    00956    'frm' are scaled by 'frmscale'. A pointer to `to` is returned. */
    00957 
    00958 /* void MtzPhiFom(MTZ *mtz); */
    00959 /* Calculates the best phase and the figure of from Hendrickson Lattman
    00960    coefficients. The following columns should be present in `mtz`:
    00961    "H", "K" & "L" (indices); "PHIB" & "FOM" (best phase (degrees) and figure of
    00962    merit); "HLA", "HLB", "HLC" & "HLD" (Hendrickson Lattman coefficients). */
    00963 
    00964 /* MTZ *MtzCopy(MTZ *frm); */
    00965 /* Produces an exact copy of `frm` and returns a pointer to it. */
    00966 
    00967 /* MTZ *MtzColAppend(MTZ *mtz, char *ident, char type); */
    00968 /* Appends a column to `*mtz` with identity `ident` and type `type`, provided
    00969    no column with identity `ident` exists. */
    00970 
    00971 /* MTZ *MtzColRemove(MTZ *mtz, char *ident); */
    00972 /* Removes a column from `*mtz` with identity `ident`. */
    00973 
    00974 /* MTZ *MtzUpdateRanges(MTZ *mtz); */
    00975 /* Updates ranges of all columns in `mtz` and returns `mtz`. */
    00976 
    00977 /* MTZCOL *MtzColNewRange(MTZCOL *col, int nref); */
    00978 /* Updates the minimum & maximum values in `col` and returns `col`. */
    00979 
    00980 /* int *MtzUnique(MTZ *mtz, char *ident); */
    00981 /* Returns an array (k) of indices: k[j] returns the first occurrence of a set
    00982    of idents, eg. MtzUnique(mtz, "H K L") returns an array from which all the 
    00983    unique reflections can be determined by indexing with k: k[i] is the index
    00984    of the last relection of a set which all have the same hkl indices. It is
    00985    assumed that `mtz` is sorted, using `ident` as keys. */
    00986 
    00987 /* float PhaseProb(float phase, float hla, float hlb, float hlc, float hld,
    00988                 BOOLEAN centric); */
    00989 /* Returns the probability of `phase` (expressed in radians) as determined by
    00990    the Hendrickson-Lattman coefficients `hla`, `hlb`, `hlc` and `hld`. If
    00991    `centric == TRUE`, the coefficients describe a centric distribution. */
    00992 
    00993 #ifdef __cplusplus
    00994 } }
    00995 #endif
    00996 #endif

.. raw:: html

   </div>
