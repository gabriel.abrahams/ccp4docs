|plots listing| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Sample plots
============

Below are examples of the **PROCHECK** plots for a protein structure
called here *1abc*.

Each example contains a brief **Description** of the plot and the main
**Options** that can be selected via the parameter file,
**procheck.prm** (described in `Customizing the **PROCHECK**
plots <../man5.html>`__).

By clicking on the plot example given you will get a larger version of
it.

-  `1. Ramachandran plot <plot_01.html>`__
-  `2. Ramachandran plots by residue type <plot_02.html>`__
-  `3. Chi1-Chi2 plots <plot_03.html>`__
-  `4. Main-chain parameters <plot_04.html>`__
-  `5. Side-chain parameters <plot_05.html>`__
-  `6. Residue properties <plot_06.html>`__
-  `7. Main-chain bond length distributions <plot_07.html>`__
-  `8. Main-chain bond angle distributions <plot_08.html>`__
-  `9. RMS distances from planarity <plot_09.html>`__
-  `10. Distorted geometry plots <plot_10.html>`__

In addition to the plots shown above, a detailed **residue-by-residue
listing** and a single **summary page** are produced as ASCII text files
(see `Appendix D <../manappd.html>`__).

--------------

|plots listing| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |plots listing| image:: ../uupb.gif
   :target: ../index.html
.. | | image:: ../12p.gif

