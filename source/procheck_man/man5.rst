|previous section| | | |contents page| | | |next section| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

5. Customizing the PROCHECK plots
=================================

The plots produced by **PROCHECK** (see `Sample
plots <examples/index.html>`__) can be customised by amending the
parameter file called `**procheck.prm** <parameters/procheck.prm>`__.
The file is created in the default directory when you first run
**PROCHECK**. You can then edit it with any text editor.

The file contains a number of *keywords* below which the parameters are
entered. The keywords are:-

-  `**Colour all plots?** <parameters/manopt_0a.html>`__
-  `**Which plots to produce** <parameters/manopt_0b.html>`__

   Plot parameters:-

   -  `**1. Ramachandran plot** <parameters/manopt_01.html>`__
   -  `**2. Gly & Pro Ramachandran
      plots** <parameters/manopt_02.html>`__
   -  `**3. Chi1-Chi2 plots** <parameters/manopt_03.html>`__
   -  `**4. Main-chain parameters** <parameters/manopt_04.html>`__
   -  `**5. Side-chain parameters** <parameters/manopt_05.html>`__
   -  `**6. Residue properties** <parameters/manopt_06.html>`__
   -  `**7. Main-chain bond length
      distributions** <parameters/manopt_07.html>`__
   -  `**8. Main-chain bond angle
      distributions** <parameters/manopt_08.html>`__
   -  `**9. RMS distances from
      planarity** <parameters/manopt_09.html>`__
   -  `**10. Distorted geometry plots** <parameters/manopt_10.html>`__

-  `**Listing options** <parameters/manopt_list.html>`__
-  `**Colours** <parameters/manopt_cols.html>`__
-  `**G-factors** <parameters/manopt_gf.html>`__
-  `**File-handles** <parameters/manopt_file.html>`__

Other text can be inserted anywhere in the file and will be ignored
provided that it is not in the block of parameters directly associated
with one of these keywords.

Throughout the file are various notes to assist you during editing.

--------------

|previous section| | | |contents page| | | |next section| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous section| image:: leftb.gif
   :target: man4.html
.. | | image:: 12p.gif
.. |contents page| image:: uupb.gif
   :target: index.html
.. |next section| image:: rightb.gif
   :target: man6.html
