|previous page| | | |plot parameters| | |\ *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

File-handles
============

The final options in the parameter file relate to the **naming** and
**labelling** of the **PostScript files** produced by the programs.

The **3** parameters are:-

--------------

::

    File-handles
    ------------
    N   <- Add 9-character description of plot to each plot filename (Y/N)?
           (eg p1nmr_01_ramachand.ps, rather than p1nmr_01.ps)
    Y   <- Print name of plotfile in bottom-left corner of plot (Y/N)?
    Y   <- Combine all pages of same plot into single PostScript file (Y/N)?

--------------

Description of options:-
------------------------

-  **Add 9-character description to plot filename** - The first option
   enables the identities of the output **PostScript** files to be more
   obvious. Thus, rather than name the files **\_01.ps**, **\_02.ps**,
   **\_03.ps**, *etc* each filename contains a **9**-character
   descriptor in addition to the number.

   The descriptors are as follows:-

   -  **ramachand** - *1. Ramachandran plot*
   -  **ramglypro** - *2. Gly & Pro Ramachandran plots*
      **allramach** - *(where all 20 Ramachandran plots are produced)*
   -  **chi1\_chi2** - *3. Chi1-Chi2 plots*
   -  **mainchpar** - *4. Main-chain parameters*
   -  **sidechpar** - *5. Side-chain parameters*
   -  **residprop** - *6. Residue properties*
   -  **bondlenth** - *7. Main-chain bond length distributions*
   -  **bondangle** - *8. Main-chain bond angle distributions*
   -  **planargps** - *9. RMS distances from planarity*
   -  **distortgm** - *10. Distorted geometry plots*

   Thus, for example, the **PostScript** file containing the
   **Ramachandran plot** might be called **p1abc\_01\_ramachand.ps**.

-  **Print name of plotfile in bottom-left corner of plot** - This
   option allows the name of the **PostScript** file to be printed in
   the bottom-left corner of plot. This helps identify which files
   contain which plots.
-  **Combine all pages of same plot into single PostScript file** - The
   default option is to create a single **PostScript** file for each
   different plot. Where the sequence is a long one, some plots may
   spill onto several pages. This option allows you to have each of
   these pages in separate **PostScript** files. It is particularly
   useful where the files generated may be too large for a given printer
   or display program to handle.

--------------

|previous page| | | |plot parameters| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous page| image:: ../leftr.gif
   :target: manopt_gf.html
.. | | image:: ../12p.gif
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
