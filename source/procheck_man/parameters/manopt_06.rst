|previous page| | | |plot parameters| | | |next page| | |\ *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

6. Residue properties
=====================

There are **18** parameters defining the `**Residue
properties** <../examples/plot_06.html>`__ plot, as follows:-

--------------

::

    6. Residue properties
    ---------------------
    1 2 3    < Which 3 main graphs to be printed (see Note 1 for full list)
    Y     <- Background shading on main graphs (Y/N)?
    2.0   <- Number of standard deviations for highlighting
    Y     <- Show shading representing estimated accessibility (Y/N)?
    N     <- Produce a COLOUR PostScript file (Y/N)?
    WHITE        <- Background colour
    CREAM        <- Background shading on main graphs
    PURPLE       <- Colour of histogram bars on main graphs
    RED          <- Colour of highlighted histogram bars
    BLUE         <- Minimum accessibility colour (buried regions)
    WHITE        <- Maximum accessibility colour
    RED          <- Region 0: Disallowed
    PINK         <- Region 1: Generous
    GREEN        <- Region 2: Allowed
    SKY BLUE     <- Region 3: Most favourable, core, region
    YELLOW       <- Colour for favourable G-factor scores
    RED          <- Colour for unfavourable G-factor scores
    YELLOW       <- Colour for schematic of the secondary structure

--------------

Description of options:-
------------------------

-  **Which 3 main graphs to be printed** - This option defines which
   **3** out of the **14** possible graphs are to appear as the three
   main graphs at the top of the page. The **14** possibilities are:

   Entering **0** for any of the 3 options will leave a **blank region**
   in place of a graph at the corresponding position on the page.

   By default, the first 3 graphs in the list will be plotted.

-  **Background shading** - This option defines whether the background
   of each of the 3 main plots is to be lightly shaded when the plot is
   in black-and-white.
-  **Number of standard deviations for highlighting** - This option
   determines which histogram bars in the 3 main plots are to
   highlighted. Residues whose plotted parameter deviates by more than
   the number of standard deviations specified here will be highlighted.
-  **Show shading for accessibility** - This option determines whether
   the secondary structure diagram is to be plotted on a shaded
   background in which the shading shows the buried and accessible
   regions of the structure. At present the accessibility is estimated
   from each residue's **Ooi** number `(Nishikawa & Ooi,
   1986) <../manrefs.html#OOI>`__, but a proper accessibility
   calculation will be included in **PROCHECK** very soon.
-  **Produce a COLOUR PostScript file** - This option defines whether a
   colour or black-and-white plot is required. Note that if the `Colour
   all plots <manopt_0a.html>`__ option is set to **Y**, a colour
   PostScript file will be produced irrespective of the setting here.

   The colour definitions on the following lines use the \`names' of
   each colour as defined in the colour table at the bottom of the
   parameter file (see `Colours <manopt_cols.html>`__). If the name of a
   colour is mis-spelt, or does not appear in the colour table, then
   **white** will be taken instead. Each colour can be altered to suit
   your taste and aesthetic judgement, as described in the
   `Colours <manopt_cols.html>`__ section.

-  **Background colour** - This option defines the background colour of
   the page on which the plots are drawn.
-  **Colours** - The various additional colours on the lines that follow
   define the colours for for the different parts of the plot. These
   include the colouring of the 3 main graphs at the top of the plot,
   the accessibility shading and colour of the secondary structure
   diagram, the markers representing the different regions of the
   Ramachandran plot, and the range of colours used on the chequer-board
   of the various *G*-factors.

--------------

|previous page| | | |plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous page| image:: ../leftr.gif
   :target: manopt_05.html
.. | | image:: ../12p.gif
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_07.html
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_07.html
