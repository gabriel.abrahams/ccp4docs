|previous section| | | |contents page| | | |next section| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

2. How to run PROCHECK
======================

To run **PROCHECK**, type the following:-

where:

-  *coordinate\_file* is the name of the file containing your protein
   structure and should include the full path unless the file is in the
   default directory. The file must be in Brookhaven format (see
   `Appendix B <manappb.html>`__).
-  *[chain-ID]* is an optional parameter used for multiple chain files
   when only one of the chains is to be analysed. If all chains are
   required, or the file only contains a single chain, you omit this
   parameter. Otherwise enter the single-letter chain ID of the chain to
   be analysed.
-  *resolution* is the **resolution** at which the structure has been
   determined. If the resolution is unknown or not relevant (*eg* for
   structures solved by **NMR**) enter any resolution (*eg* **2.0**).

For example, to run the check on the Brookhaven file *1abc*, which was
solved to **2.4Å** you might enter:

Running just the plotting programs
----------------------------------

The **PROPLOT** script runs just the plotting programs that generate the
output PostScript files. It skips the programs that calculate the data,
and so is much faster than running **PROCHECK** all over again. (Of
course, the calculation programs need to have been run at least once so
that all the required data files are present). The script is
particularly useful if you want to regenerate some, or all, of the
plots, perhaps with different plot parameters in the **procheck.prm**
file (see `Customizing the **PROCHECK** plots <man5.html>`__).

To run **PROPLOT**, enter:

where the parameters are the same as before.

Running in batch-mode on a VAX
------------------------------

On VAX computers, you can submit the entire process to a batch queue by
entering:-

where the first three parameters are as before, and:

-  *queue-name* is the name of the batch-queue to which you want the job
   submitted.
-  *default-directory* is the directory to which the files created by
   **PROCHECK** are to be written. Note, this need not be the same
   directory as the one containing the structure file, but it must be
   one to which you have write access.

If you just type **prosub** you will be prompted for each of these **5**
parameters in turn. If you do not want to specify a **chain ID**, leave
this entry blank.

--------------

|previous section| | | |contents page| | | |next section| | | *PROCHECK Operating Manual*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous section| image:: leftb.gif
   :target: man1.html
.. | | image:: 12p.gif
.. |contents page| image:: uupb.gif
   :target: index.html
.. |next section| image:: rightb.gif
   :target: man3.html
