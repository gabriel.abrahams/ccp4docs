G-factors - All atoms

G-factors - All atoms
---------------------

--------------

|image0|

--------------

The figure above shows all atoms colours according to the *G*-factors of
the corresponding torsion angles. Red regions indicate places that may
need closer examination as they signify unusual geometry.

[The figure was generated using **QUANTA**].

.. |image0| image:: man6a.gif

